# VaultUI

VaultUI is a user interface for [Hashicorp Vault](https://www.hashicorp.com/products/vault).

## Supported Features:
### Unseal
- Can unseal using a QR code making it easier for multi-user teams to unseal vault together.
### Key Value
- Viewing of Key/Value secrets (v1 & v2)
- Managing versions of secrets in v2 mounts.
- Editing and Viewing Key/Value secrets in JSON, JSON5, Yaml, etc with default settings for both.
- Search bar for easily finding a secret on mobile.
- Can add a `__vaultui_totp_path` attribute (example: `__vaultui_totp_path: "totp/gitlab"`) to KV secrets to add a link for viewing of affiliated TOTP paths.
### TOTP
- Can view and delete TOTP codes.
- Ability to scan a QR code, input a URI or input a key for adding TOTP codes.
- Supports adding vault-generated TOTP keys for testing.
### Transit
- Can create transit engines and keys with all supported cyphers.
- Supports encrypt, decrypt and rewrap in browser.
### Policies
- Can view, edit and delete policies.
### Auth
- Supports limited viewing some auth methods.
#### Auth / UserPass
- Can manage userpass users and change passwords, expiry and other settings

## How to build:

Recommended: use direnv or just nix-shell to automatically grab nodejs and required build dependencies.

```
npm install --save-dev
npx webpack
```
The resulting built files will be in `dist/`

## How to run in development:
```
npm install --save-dev
npx webpack serve --config webpack-dev.config.js
```

## How to host/use

Simply copy the built files to root of webserver.
You will need a redirect rule to redirect any 404's or non-200's to index.html. 

Cors will need to be enabled for you to use VaultUI.
Due to vault encrypting cors config with the unseal key, you can't use VaultUI to unseal vault for the first time.

However, you can use VaultUI to unseal vault for first unseal if you can replace without appending to the cors headers so they use the same headers when unsealed as sealed.


### Redirects
#### Netlify Redirect Rule:
```
/* /index.html 200
```
### Nginx
```nginx
location / {
    try_files $uri $uri/ /index.html;
}
```
### AWS Amplify
original address: `</^[^.]+$|\.(?!(css|gif|ico|jpg|js|png|txt|svg|woff|woff2|ttf|map|json)$)([^.]+$)/>`
destination address: `/index.html`
redirect type: `200`


## Translating
Make sure to keep the order of comments and tags in the translation files `src/translations` the same.

When you want to MR an update, title it "Changed wording in..." or "Synced translations in de" or something along those lines.

To get a list of what languages need what translations added, run `node ./checkTranslations.mjs`

For example:
```
Language: fr
Missing:  unseal_input_btn, unseal_qr_btn
```
Would mean those two missing translations need to be added to the french translation.
