"use strict";

// JS & CSS

/* eslint-disable */
import UIkit from "uikit";
// Don't Sort These!
import Icons from "uikit/dist/js/uikit-icons";
// @ts-ignore
UIkit.use(Icons);

import Prism from "prismjs";
// Don't Sort These!
import "prismjs/components/prism-hcl";
import "prismjs/components/prism-javascript";
import "prismjs/components/prism-json";
import "prismjs/components/prism-json5";
import "prismjs/components/prism-yaml";

Prism.highlightAll();
/* eslint-enable */

// @ts-ignore
import translations from "./translations/index.mjs";

// Actual Imports
import { Main } from "./pages";
import { NavBar } from "./ui/elements/NavBar";
import { ThemeLoader } from "./ThemeLoader";
import { api } from "./globals/globalAPI";
import { formatDistance } from "./utils/formatDistance";
import { getCurrentUrl, route } from "preact-router";
import { pageChecks } from "./pageChecks";
import { playground } from "./playground";
import { render } from "preact";
import { settings } from "./globals/globalSettings";
import i18next from "i18next";

async function onLoad(): Promise<void> {
  document.documentElement.dir = settings.pageDirection;

  render(
    <>
      <ThemeLoader />
      <NavBar />
      <div class="uk-container uk-container-medium uk-align-center">
        <div class="uk-card uk-card-body">
          <Main />
        </div>
      </div>
    </>,
    document.body,
  );

  await pageChecks(getCurrentUrl(), api, settings);

  if (process.env.NODE_ENV == "development") {
    await playground();
  }

  setInterval(async () => {
    if (getCurrentUrl() != "/unseal") {
      // TODO: check if api is accessable, not if its not set
      if (settings.apiURL.length == 0) {
        return;
      }
      const sealStatus = await api.getSealStatus();
      if (sealStatus.sealed) {
        route("/unseal", false);
        return;
      }
    }
  }, 5000);
}

document.addEventListener(
  "DOMContentLoaded",
  async () => {
    console.log("Loading...");
    await i18next.init({
      lng: settings.language,
      fallbackLng: "en",
      debug: true,
      // @ts-ignore
      resources: Object.fromEntries(
        Object.entries(translations).map(([k, v]) => [k, { translation: v }]),
      ),
      interpolation: {
        escape: (str) => {
          return str;
        },
        format: function (value: unknown, format, _): string {
          if (format === "until_date" && value instanceof Date)
            return formatDistance(new Date(), new Date(value), settings.language);
          return value as string;
        },
      },
    });
    await onLoad();
  },
  false,
);
