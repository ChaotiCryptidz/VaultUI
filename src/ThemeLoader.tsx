import { Component } from "preact";
import { settings } from "./globals/globalSettings";

// @ts-ignore
import style_dark from "./scss/main-dark.scss" assert { type: "css" };
// @ts-ignore
import style_light from "./scss/main-light.scss" assert { type: "css" };

export const default_theme = "dark";

const themes: { [key: string]: string } = {
  dark: style_dark as string,
  light: style_light as string,
};

export class ThemeLoader extends Component<unknown, { sheet: string }> {
  componentDidMount() {
    this.setCorrectStyle(settings.theme);
    settings.registerListener((key: string) => {
      if (key != "theme") return;
      this.setCorrectStyle(settings.theme);
    });
  }

  setCorrectStyle(theme: string) {
    this.setState({ sheet: themes[theme] });
  }

  render() {
    if (!this.state.sheet) return;
    return <style>{this.state.sheet}</style>;
  }
}
