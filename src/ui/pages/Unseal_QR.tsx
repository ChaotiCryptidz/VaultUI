import { QRScanner } from "../elements/QRScanner";

type QRUnsealProps = {
  onKeySubmit: (key: string) => void;
};

export function UnsealQR(props: QRUnsealProps) {
  return <QRScanner onScan={(code: string) => props.onKeySubmit(code)} />;
}
