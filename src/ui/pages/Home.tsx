import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { Grid, GridSizes } from "../elements/Grid";
import { Margin } from "../elements/Margin";
import { PageTitle } from "../elements/PageTitle";
import { Tile } from "../elements/Tile";
import { TokenInfo } from "../../api/types/token";
import { pageChecks } from "../../pageChecks";
import { route } from "preact-router";
import { sendErrorNotification } from "../elements/ErrorMessage";
import i18next from "i18next";

type HomeState = {
  selfTokenInfo: TokenInfo;
  authCaps: string[];
  policiesCaps: string[];
};

export class Home extends Component<DefaultPageProps, HomeState> {
  async componentDidMount() {
    // Always call pageChecks on /home
    if (await pageChecks("/home", this.props.api, this.props.settings)) return;

    try {
      await this.populateState();
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  async populateState() {
    // Check if logged in otherise redirect to /login
    let selfTokenInfo: TokenInfo;
    try {
      selfTokenInfo = await this.props.api.lookupSelf();
    } catch (e: unknown) {
      const error = e as Error;

      if (error.message == "permission denied") {
        this.props.settings.token = "";
        route("/login", true);
        return;
      } else {
        throw error;
      }
    }

    const caps = await this.props.api.getCapabilitiesPath(["sys/auth", "sys/policies"]);
    const authCaps = caps["sys/auth"];
    const policiesCaps = caps["sys/policies"];

    this.setState({
      selfTokenInfo: selfTokenInfo,
      authCaps: authCaps,
      policiesCaps: policiesCaps,
    });
  }

  render(): JSX.Element {
    return (
      this.state.selfTokenInfo && (
        <>
          <PageTitle title={i18next.t("home_page_title")} />
          <div>
            <ul id="textList" class="uk-nav">
              <li>
                <span>{i18next.t("home_vaulturl_text", { text: this.props.settings.apiURL })}</span>
              </li>
              <li>
                <a href="/pw_gen">{i18next.t("home_password_generator_btn")}</a>
              </li>
              <li>
                <span>
                  {i18next.t("home_your_token_expires_in", {
                    date: new Date(this.state.selfTokenInfo.expire_time),
                  })}
                </span>
              </li>
            </ul>
            <Margin>
              <Grid size={GridSizes.MATCHING_TWO_ROWS}>
                <Tile
                  title={i18next.t("home_secrets_title")}
                  description={i18next.t("home_secrets_description")}
                  icon="file-edit"
                  href="/secrets"
                />
                <Tile
                  title={i18next.t("home_access_title")}
                  description={i18next.t("home_access_description")}
                  icon="users"
                  disabled={!this.state.authCaps.includes("read")}
                  href="/access"
                />
                <Tile
                  title={i18next.t("home_policies_title")}
                  description={i18next.t("home_policies_description")}
                  icon="pencil"
                  disabled={!this.state.policiesCaps.includes("read")}
                  href="/policies"
                />
              </Grid>
            </Margin>
          </div>
        </>
      )
    );
  }
}
