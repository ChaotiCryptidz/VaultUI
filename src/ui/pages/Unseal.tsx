import { Button } from "../elements/Button";
import { Component, JSX, createRef } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { ErrorMessage } from "../elements/ErrorMessage";
import { Margin } from "../elements/Margin";
import { PageTitle } from "../elements/PageTitle";
import { UnsealForm } from "./Unseal_Form";
import { UnsealQR } from "./Unseal_QR";
import { route } from "preact-router";
import { toStr } from "../../utils";
import i18next from "i18next";

const UnsealInputModes = {
  FORM_INPUT: "FORM_INPUT",
  QR_INPUT: "QR_INPUT",
};

type UnsealPageState = {
  mode: string;
  keys_submitted: number;
  keys_needed: number;
  timer: number;
};

export class Unseal extends Component<DefaultPageProps, UnsealPageState> {
  constructor() {
    super();
    this.state = {
      mode: UnsealInputModes.FORM_INPUT,
      keys_submitted: 0,
      keys_needed: 0,
      timer: 0,
    };
  }

  async submitKey(key: string): Promise<void> {
    try {
      await this.props.api.submitUnsealKey(key);
      this.updateStateWithSealStatus();
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }

  updateStateWithSealStatus(): void {
    void this.props.api.getSealStatus().then((data) => {
      this.setState({
        keys_submitted: data.progress,
        keys_needed: data.t,
      });
      if (!data.sealed) {
        route("/");
      }
    });
  }

  componentWillUnmount(): void {
    clearInterval(this.state.timer);
  }

  componentDidMount(): void {
    this.updateStateWithSealStatus();
    const timer = setInterval(() => {
      this.updateStateWithSealStatus();
    }, 500) as unknown as number;
    this.setState({ timer: timer });
  }

  errorMessageRef = createRef<ErrorMessage>();

  render(): JSX.Element {
    return (
      <>
        <PageTitle title={i18next.t("unseal_vault_text")} />
        <div>
          <progress
            class="uk-progress"
            value={this.state.keys_submitted}
            max={this.state.keys_needed}
          />

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <p>
            {i18next.t("unseal_keys_progress", {
              progress: toStr(this.state.keys_submitted),
              keys_needed: toStr(this.state.keys_needed),
            })}
          </p>

          {this.state.mode == UnsealInputModes.FORM_INPUT && (
            <UnsealForm onKeySubmit={(code) => this.submitKey(code)} />
          )}

          {this.state.mode == UnsealInputModes.QR_INPUT && (
            <UnsealQR onKeySubmit={(code) => this.submitKey(code)} />
          )}

          <Button
            text={
              this.state.mode == UnsealInputModes.QR_INPUT
                ? i18next.t("unseal_input_btn")
                : i18next.t("unseal_qr_btn")
            }
            color="primary"
            onClick={async () => {
              let newMethod: string;
              if (this.state.mode == UnsealInputModes.FORM_INPUT) {
                newMethod = UnsealInputModes.QR_INPUT;
              } else {
                newMethod = UnsealInputModes.FORM_INPUT;
              }
              this.setState({ mode: newMethod });
            }}
          />
        </div>
      </>
    );
  }
}
