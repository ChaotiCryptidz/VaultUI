import { Button } from "../elements/Button";
import { Component } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { Form } from "../elements/forms/Form";
import { InputWithTitle } from "../elements/InputWithTitle";
import { Margin } from "../elements/Margin";
import { PageTitle } from "../elements/PageTitle";
import { TextInput } from "../elements/forms/TextInput";
import { route } from "preact-router";
import i18next from "i18next";

export class SetVaultURL extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title={i18next.t("set_vault_url_title")} />
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <Margin>
            <InputWithTitle title={i18next.t("set_vault_url_placeholder")}>
              <TextInput
                name="vaultURL"
                placeholder={i18next.t("set_vault_url_placeholder")}
                required
              />
            </InputWithTitle>
          </Margin>

          <Margin>
            <Button text={i18next.t("set_vault_url_set_btn")} color="primary" type="submit" />
          </Margin>
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    // TODO: check if vault is actually working here.
    this.props.settings.apiURL = data.get("vaultURL") as string;
    route("/");
  }
}
