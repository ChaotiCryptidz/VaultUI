import { Button } from "../../../elements/Button";
import { Checkbox } from "../../../elements/forms/Checkbox";
import { Component, JSX, createRef } from "preact";
import { CopyableInputBox } from "../../../elements/CopyableInputBox";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../elements/ErrorMessage";
import { Form } from "../../../elements/forms/Form";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Margin } from "../../../elements/Margin";
import { MarginInline } from "../../../elements/MarginInline";
import { NewTOTPResp } from "../../../../api/types/totp";
import { NumberInput } from "../../../elements/forms/NumberInput";
import { SecretTitleElement } from "../SecretTitleElement";
import { Select, SelectOption } from "../../../elements/forms/Select";
import { TextInput } from "../../../elements/forms/TextInput";
import { route } from "preact-router";
import { totpListURL } from "../../pageLinks";
import i18next from "i18next";

export class TOTPNewGeneratedForm extends Component<
  { baseMount: string } & DefaultPageProps,
  { exportedData: NewTOTPResp }
> {
  errorMessageRef = createRef<ErrorMessage>();
  uriInputRef = createRef<HTMLInputElement>();

  render(): JSX.Element {
    if (!this.state.exportedData) {
      return (
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <Margin>
            <InputWithTitle title={i18next.t("common_name")}>
              <TextInput name="name" placeholder={i18next.t("common_name")} required />
            </InputWithTitle>
          </Margin>

          <Margin>
            <InputWithTitle title={i18next.t("totp_new_generated_issuer")}>
              <TextInput name="issuer" required />
            </InputWithTitle>
          </Margin>

          <Margin>
            <InputWithTitle title={i18next.t("totp_new_generated_account_name")}>
              <TextInput name="account_name" required />
            </InputWithTitle>
          </Margin>

          <Margin>
            <InputWithTitle title={i18next.t("totp_new_generated_algorithm")}>
              <Select name="algorithm">
                {["SHA512", "SHA256", "SHA1"].map((type) => (
                  <SelectOption name={type} value={type} />
                ))}
              </Select>
            </InputWithTitle>
          </Margin>

          <Margin>
            <InputWithTitle title={i18next.t("totp_new_generated_key_size")}>
              <NumberInput name="key_size" value="20" />
            </InputWithTitle>
          </Margin>

          <Margin>
            <InputWithTitle title={i18next.t("totp_new_generated_period")}>
              <TextInput name="period" value="30s" />
            </InputWithTitle>
          </Margin>

          <Margin>
            <InputWithTitle title={i18next.t("totp_new_generated_digits")}>
              <NumberInput name="digits" value="6" />
            </InputWithTitle>
          </Margin>

          <Margin>
            <InputWithTitle title={i18next.t("totp_new_generated_export")}>
              <Checkbox name="exported" checked />
            </InputWithTitle>
          </Margin>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <MarginInline>
            <Button text={i18next.t("common_create")} color="primary" type="submit" />
          </MarginInline>
        </Form>
      );
    } else {
      return (
        <>
          <p>{i18next.t("totp_new_generated_warning")}</p>
          <img src={"data:image/png;base64," + this.state.exportedData.barcode} />
          <CopyableInputBox copyable text={this.state.exportedData.url} />
          <Button
            text={i18next.t("common_back")}
            color="primary"
            route={totpListURL(this.props.baseMount)}
          />
        </>
      );
    }
  }

  async onSubmit(data: FormData): Promise<void> {
    const isExported = data.get("exported") == "yes" ? true : false;

    const parms = {
      generate: true,
      name: data.get("name") as string,
      issuer: data.get("issuer") as string,
      account_name: data.get("account_name") as string,
      exported: isExported,
      key_size: parseInt(data.get("key_size") as string),
      period: data.get("period") as string,
      algorithm: data.get("algorithm") as string,
      digits: parseInt(data.get("digits") as string),
    };

    try {
      const ret = await this.props.api.addNewTOTP(this.props.baseMount, parms);
      if (!isExported) {
        route("/secrets/totp/list/" + this.props.baseMount);
      } else {
        this.setState({ exportedData: ret });
      }
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }
}

export class TOTPNewGenerated extends Component<DefaultPageProps> {
  render() {
    const baseMount = this.props.matches["baseMount"];
    return (
      <>
        <SecretTitleElement
          type="totp"
          baseMount={baseMount}
          suffix={i18next.t("totp_new_generated_suffix")}
        />
        <TOTPNewGeneratedForm
          settings={this.props.settings}
          api={this.props.api}
          baseMount={baseMount}
        />
      </>
    );
  }
}
