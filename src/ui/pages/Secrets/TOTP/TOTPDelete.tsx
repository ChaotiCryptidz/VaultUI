import { Button } from "../../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../elements/ErrorMessage";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { route } from "preact-router";
import { totpListURL } from "../../pageLinks";
import i18next from "i18next";

export class TOTPDelete extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  render() {
    return (
      <>
        <SecretTitleElement
          type="totp"
          baseMount={this.props.matches["baseMount"]}
          item={this.props.matches["item"]}
          suffix={i18next.t("totp_delete_suffix")}
        />
        <div>
          <h5>{i18next.t("totp_delete_text")}</h5>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <Button
            text={i18next.t("common_delete")}
            color="danger"
            onClick={async () => {
              const baseMount = this.props.matches["baseMount"];
              const item = this.props.matches["item"];
              try {
                await this.props.api.deleteTOTP(baseMount, item);
                route(totpListURL(baseMount));
              } catch (e: unknown) {
                const error = e as Error;
                this.errorMessageRef.current.setErrorMessage(error.message);
              }
            }}
          />
        </div>
      </>
    );
  }
}
