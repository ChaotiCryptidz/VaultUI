import { Button } from "../../../elements/Button";
import { CapabilitiesType } from "../../../../api/types/capabilities";
import { Component, JSX, createRef } from "preact";
import { CopyableInputBox } from "../../../elements/CopyableInputBox";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { Grid, GridSizes } from "../../../elements/Grid";
import { InlineButtonBox } from "../../../elements/InlineButtonBox";
import { MarginInline } from "../../../elements/MarginInline";
import { SecretTitleElement } from "../SecretTitleElement";
import { TextInput } from "../../../elements/forms/TextInput";
import {
  delSecretsEngineURL,
  totpDeleteURL,
  totpNewGeneratedURL,
  totpNewURL,
} from "../../pageLinks";
import { removeDoubleSlash } from "../../../../utils";
import { sendErrorNotification } from "../../../elements/ErrorMessage";
import i18next from "i18next";

type TOTPGridItemProps = DefaultPageProps & {
  baseMount: string;
  totpKey: string;
  canDelete: boolean;
};

export class RefreshingTOTPGridItem extends Component<TOTPGridItemProps, { totpValue: string }> {
  constructor() {
    super();
    this.state = { totpValue: "" };
  }
  timer: unknown;

  updateTOTPCode(): void {
    void this.props.api.getTOTPCode(this.props.baseMount, this.props.totpKey).then((code) => {
      this.setState({ totpValue: code });
    });
  }

  componentWillUnmount(): void {
    clearInterval(this.timer as number);
  }

  componentDidMount(): void {
    this.updateTOTPCode();
    this.timer = setInterval(() => {
      this.updateTOTPCode();
    }, 3000);
  }

  render(): JSX.Element {
    return (
      <Grid size={GridSizes.NORMAL}>
        <CopyableInputBox text={this.props.totpKey} copyable />
        <CopyableInputBox text={this.state.totpValue} copyable />
        <div>
          <MarginInline>
            {this.props.canDelete && (
              <Button
                text={i18next.t("common_delete")}
                color="danger"
                route={totpDeleteURL(this.props.baseMount, this.props.totpKey)}
              />
            )}
          </MarginInline>
        </div>
      </Grid>
    );
  }
}

type TOTPItem = {
  totpKey: string;
  canDelete: boolean;
};

type TOTPListViewProps = DefaultPageProps & {
  totpItems: TOTPItem[];
};

type TOTPListViewState = {
  searchQuery: string;
};

export class TOTPListView extends Component<TOTPListViewProps, TOTPListViewState> {
  constructor() {
    super();
    this.state = {
      searchQuery: "",
    };
  }

  searchBarRef = createRef<HTMLInputElement>();

  componentDidMount(): void {
    this.setState({
      searchQuery: "",
    });
  }

  render() {
    if (this.props.totpItems.length == 0) {
      return <p>{i18next.t("totp_list_empty")}</p>;
    }

    return (
      <>
        <br />

        <TextInput
          inputRef={this.searchBarRef}
          name="path"
          placeholder={i18next.t("totp_list_search_input_text")}
          onInput={async () => {
            this.setState({
              searchQuery: this.searchBarRef.current.value,
            });
          }}
        />

        <br />
        <br />

        {this.props.totpItems.map((totpItem) => {
          if (this.state.searchQuery.length > 0) {
            if (!totpItem.totpKey.includes(this.state.searchQuery)) {
              return;
            }
          }
          return (
            <RefreshingTOTPGridItem
              {...this.props}
              baseMount={this.props.matches["baseMount"]}
              {...totpItem}
            />
          );
        })}
      </>
    );
  }
}

type TOTPListState = {
  capabilities?: CapabilitiesType;
  totpItems: TOTPItem[];
};

export class TOTPList extends Component<DefaultPageProps, TOTPListState> {
  constructor() {
    super();
    this.state = { capabilities: null, totpItems: [] };
  }

  async doApiFetches() {
    const api = this.props.api;
    const baseMount = this.props.matches["baseMount"];
    const mountsPath = "/sys/mounts/" + baseMount;

    const caps = await api.getCapabilitiesPath([mountsPath, baseMount]);

    let totpItems: TOTPItem[] = [];

    const totpKeys = await api.getTOTPKeys(baseMount);

    const totpKeyPermissions = await Promise.all(
      Array.from(
        totpKeys.map(async (key) => {
          const totpCaps = await api.getCapsPath(removeDoubleSlash(baseMount + "/code/" + key));
          return { key: key, caps: totpCaps };
        }),
      ),
    );

    totpItems = Array.from(
      totpKeyPermissions.map((keyData) => {
        // Filter out all non-readable totp keys.
        if (!keyData.caps.includes("read")) return;
        return {
          totpKey: keyData.key,
          canDelete: keyData.caps.includes("delete"),
        };
      }),
    );

    this.setState({
      capabilities: caps,
      totpItems: totpItems,
    });
  }

  async componentDidMount() {
    try {
      await this.doApiFetches();
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    if (!this.state.capabilities) return;
    const baseMount = this.props.matches["baseMount"];

    const mountsPath = "/sys/mounts/" + baseMount;
    const mountCaps = this.state.capabilities[mountsPath];
    const totpCaps = this.state.capabilities[baseMount];

    const secretItem = this.props.matches["secretItem"] || null;
    const secretItemSet = secretItem != null && typeof secretItem == "string";

    return (
      <>
        <SecretTitleElement type="totp" baseMount={baseMount} item={secretItem} />
        {!secretItemSet && (
          <div>
            <InlineButtonBox>
              {totpCaps.includes("create") && (
                <Button
                  text={i18next.t("common_new")}
                  color="primary"
                  route={totpNewURL(baseMount)}
                />
              )}
              {totpCaps.includes("create") && (
                <Button
                  text={i18next.t("totp_new_generated")}
                  color="secondary"
                  route={totpNewGeneratedURL(baseMount)}
                />
              )}
              {mountCaps.includes("delete") && (
                <Button
                  text={i18next.t("common_delete")}
                  color="danger"
                  route={delSecretsEngineURL(baseMount)}
                />
              )}
            </InlineButtonBox>
            <div>
              <TOTPListView {...this.props} totpItems={this.state.totpItems} />
            </div>
          </div>
        )}
        {secretItemSet && (
          <div>
            <RefreshingTOTPGridItem
              {...this.props}
              baseMount={this.props.matches["baseMount"]}
              totpKey={secretItem}
              canDelete={false}
            />
            <br />
            <Button
              text={i18next.t("common_back")}
              color="primary"
              onClick={() => {
                window.history.back();
              }}
            />
          </div>
        )}
      </>
    );
  }
}
