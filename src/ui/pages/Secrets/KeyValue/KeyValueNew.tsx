import { Button } from "../../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../elements/ErrorMessage";
import { Form } from "../../../elements/forms/Form";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { TextInput } from "../../../elements/forms/TextInput";
import { combineKVPath, splitKVPath } from "./kvPathUtils";
import { kvEditURL } from "../../pageLinks";
import { route } from "preact-router";
import i18next from "i18next";

export class KeyValueNew extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  async newKVSecretHandleForm(
    formData: FormData,
    baseMount: string,
    secretPath: string[],
  ): Promise<void> {
    const path = formData.get("path") as string;

    let keyData: Record<string, unknown> = {};

    try {
      const mountInfo = await this.props.api.getMount(baseMount);
      if (mountInfo.options.version == "1") {
        // Can't have a empty secret on KV V1
        keyData = { placeholder_on_kv1: "placeholder_on_kv1" };
      }

      await this.props.api.createOrUpdateSecret(baseMount, secretPath, path, keyData);

      const combined = combineKVPath(secretPath, path);

      route(kvEditURL(baseMount, combined.secretPath, combined.secretItem));
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }

  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = splitKVPath(this.props.matches["secretPath"] || "");

    return (
      <>
        <SecretTitleElement
          type="kv"
          baseMount={baseMount}
          secretPath={secretPath}
          suffix={i18next.t("kv_new_suffix")}
        />
        <div>
          <Form
            onSubmit={async (formData) =>
              await this.newKVSecretHandleForm(formData, baseMount, secretPath)
            }
          >
            <Margin>
              <InputWithTitle title={i18next.t("kv_new_path")}>
                <TextInput name="path" required />
              </InputWithTitle>
            </Margin>

            <Margin>
              <ErrorMessage ref={this.errorMessageRef} />
            </Margin>

            <Button text={i18next.t("common_create")} color="primary" type="submit" />
          </Form>
        </div>
      </>
    );
  }
}
