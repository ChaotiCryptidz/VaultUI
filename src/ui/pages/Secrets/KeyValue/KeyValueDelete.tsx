import { Button } from "../../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../elements/ErrorMessage";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { kvListURL, kvViewURL } from "../../pageLinks";
import { route } from "preact-router";
import { splitKVPath } from "./kvPathUtils";
import i18next from "i18next";

export class KeyValueDelete extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  async onDelete() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = splitKVPath(this.props.matches["secretPath"]);
    const item = this.props.matches["item"];
    const version = this.props.matches["version"];

    // If deleting a secret when version is null,
    // redirect back to list rather than secret
    const buttonRoute =
      version == "null"
        ? kvListURL(baseMount, secretPath)
        : kvViewURL(baseMount, secretPath, item, "null");

    try {
      await this.props.api.deleteSecret(baseMount, secretPath, item, version);
      route(buttonRoute);
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }

  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = splitKVPath(this.props.matches["secretPath"]);
    const item = this.props.matches["item"];

    return (
      <>
        <SecretTitleElement
          type="kv"
          baseMount={baseMount}
          secretPath={secretPath}
          item={item}
          suffix={i18next.t("kv_delete_suffix")}
        />
        <div>
          <h5>{i18next.t("kv_delete_text")}</h5>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <Button
            text={i18next.t("common_delete")}
            color="danger"
            onClick={async () => this.onDelete()}
          />
        </div>
      </>
    );
  }
}
