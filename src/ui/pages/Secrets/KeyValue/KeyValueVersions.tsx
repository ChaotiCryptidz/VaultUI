import { Component } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { SecretTitleElement } from "../SecretTitleElement";
import { kvViewURL } from "../../pageLinks";
import { objectToMap } from "../../../../utils";
import { sendErrorNotification } from "../../../elements/ErrorMessage";
import { splitKVPath } from "./kvPathUtils";

export class KeyValueVersions extends Component<DefaultPageProps, { versions: string[] }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = splitKVPath(this.props.matches["secretPath"]);
    console.log("sp", secretPath, this.props.matches["secretPath"]);
    const secretItem = this.props.matches["item"];

    try {
      const metadata = await this.props.api.getSecretMetadata(baseMount, secretPath, secretItem);

      const versions = Array.from(objectToMap(metadata.versions).keys());

      this.setState({ versions });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    if (!this.state.versions) return;
    const baseMount = this.props.matches["baseMount"];
    const secretPath = splitKVPath(this.props.matches["secretPath"]);
    const secretItem = this.props.matches["item"];

    return (
      <>
        <SecretTitleElement
          type="kv"
          baseMount={baseMount}
          secretPath={secretPath}
          item={this.props.matches["item"]}
        />
        <ul class="uk-nav uk-nav-default">
          {this.state.versions.map((ver) => (
            <li>
              <a href={kvViewURL(baseMount, secretPath, secretItem, ver)}>{`v${ver}`}</a>
            </li>
          ))}
        </ul>
      </>
    );
  }
}
