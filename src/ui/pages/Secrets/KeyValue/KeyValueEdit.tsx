import { Button } from "../../../elements/Button";
import { CodeEditor } from "../../../elements/CodeEditor";
import { Component, JSX, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { ErrorMessage, sendErrorNotification } from "../../../elements/ErrorMessage";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { Select, SelectOption } from "../../../elements/forms/Select";
import {
  SupportedLanguages,
  dumpData,
  parseData,
  toPrismCode,
} from "../../../../utils/dataInterchange";
import { combineKVPath, splitKVPath } from "./kvPathUtils";
import { kvViewURL } from "../../pageLinks";
import { route } from "preact-router";
import { sortedObjectMap } from "../../../../utils";
import i18next from "i18next";

export type KVEditProps = DefaultPageProps & {
  baseMount: string;
  secretPath: string[];
  secretItem: string;
};

type KVEditState =
  | {
      dataLoaded: false;
      syntax: string;
    }
  | {
      dataLoaded: true;
      kvData: Record<string, unknown>;
      code: string;
      syntax: string;
    };

export class KVEditor extends Component<KVEditProps, KVEditState> {
  constructor() {
    super();
    this.state = {
      dataLoaded: false,
      syntax: "",
    };
  }

  errorMessageRef = createRef<ErrorMessage>();

  async componentDidMount() {
    this.setState({ syntax: this.props.settings.kvEditorDefaultLanguage });
    if (!this.state.dataLoaded) {
      try {
        await this.loadData();
      } catch (e: unknown) {
        const error = e as Error;
        sendErrorNotification(error.message);
      }
    }
  }

  async loadData() {
    const kvData = await this.props.api.getSecret(
      this.props.baseMount,
      this.props.secretPath.map((e) => e + "/"),
      this.props.secretItem,
    );
    this.setState({
      dataLoaded: true,
      kvData: kvData,
      code: this.getStringKVData(kvData, this.state.syntax),
    });
  }

  async editorSave(): Promise<void> {
    if (!this.state.dataLoaded) return;
    const editorContent = this.state.code;

    try {
      parseData(editorContent, this.state.syntax);
    } catch {
      this.errorMessageRef.current.setErrorMessage(i18next.t("kv_sec_edit_invalid_data_err"));
      return;
    }

    try {
      const combined = combineKVPath(this.props.secretPath, this.props.secretItem);
      await this.props.api.createOrUpdateSecret(
        this.props.baseMount,
        combined.secretPath,
        combined.secretItem,
        parseData(editorContent, this.state.syntax),
      );
      route(kvViewURL(this.props.baseMount, this.props.secretPath, this.props.secretItem));
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }

  getStringKVData(data: Record<string, unknown>, syntax: string): string {
    return dumpData(
      Object.fromEntries(sortedObjectMap(data)),
      this.props.settings.kvEditorIndent,
      syntax,
    );
  }

  syntaxSelectRef = createRef<HTMLSelectElement>();

  render(): JSX.Element {
    if (!this.state.dataLoaded) {
      return <p>{i18next.t("content_loading")}</p>;
    }

    return (
      <div>
        <InputWithTitle title={i18next.t("kv_sec_edit_syntax")}>
          <Select
            selectRef={this.syntaxSelectRef}
            onInput={() => {
              this.setState({ syntax: this.syntaxSelectRef.current.value });
            }}
          >
            {SupportedLanguages.map((lang) => {
              return (
                <SelectOption
                  name={lang.readable}
                  value={lang.name}
                  selected={this.props.settings.kvEditorDefaultLanguage == lang.name}
                />
              );
            })}
          </Select>
        </InputWithTitle>

        <Margin>
          <ErrorMessage ref={this.errorMessageRef} />
        </Margin>

        <CodeEditor
          language={toPrismCode(this.state.syntax)}
          tabSize={this.props.settings.kvEditorIndent}
          code={this.getStringKVData(this.state.kvData, this.state.syntax)}
          onUpdate={(code) => {
            this.setState({ code });
          }}
        />

        <Button text={i18next.t("common_edit")} color="primary" onClick={() => this.editorSave()} />
      </div>
    );
  }
}

export class KeyValueEdit extends Component<DefaultPageProps> {
  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = splitKVPath(this.props.matches["secretPath"]);
    const item = this.props.matches["item"];

    return (
      <>
        <SecretTitleElement
          type="kv"
          baseMount={baseMount}
          secretPath={secretPath}
          item={this.props.matches["item"]}
          suffix={i18next.t("kv_sec_edit_suffix")}
        />
        <KVEditor
          settings={this.props.settings}
          api={this.props.api}
          baseMount={baseMount}
          secretPath={secretPath}
          secretItem={item}
        />
      </>
    );
  }
}
