type CombinedPaths = {
  secretPath: string[];
  secretItem: string;
};

export function combineKVPath(secretPath: string[], path: string): CombinedPaths {
  if (path.includes("/")) {
    const split = path.split("/");
    const secret = split.pop();
    return {
      secretPath: [...secretPath, ...split].filter((e) => e !== ""),
      secretItem: secret,
    };
  } else {
    return {
      secretPath: secretPath.filter((e) => e !== ""),
      secretItem: path,
    };
  }
}

export function splitKVPath(path: string): string[] {
  return path.split("/").filter((e) => e !== "");
}
