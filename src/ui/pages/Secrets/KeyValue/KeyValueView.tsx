import { API } from "../../../../api/API";
import { Button } from "../../../elements/Button";
import { CodeBlock } from "../../../elements/CodeBlock";
import { Component, JSX, createRef } from "preact";
import { CopyableInputBox } from "../../../elements/CopyableInputBox";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { DoesNotExistError } from "../../../../types/internalErrors";
import { Grid, GridSizes } from "../../../elements/Grid";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { SecretTitleElement } from "../SecretTitleElement";
import { Select, SelectOption } from "../../../elements/forms/Select";
import { SupportedLanguages, dumpData, toPrismCode } from "../../../../utils/dataInterchange";
import { addClipboardNotifications } from "../../../../utils/clipboardNotifs";
import { kvDeleteURL, kvEditURL, kvVersionsURL, totpListURL } from "../../pageLinks";
import { sendErrorNotification } from "../../../elements/ErrorMessage";
import { sortedObjectMap } from "../../../../utils";
import { splitKVPath } from "./kvPathUtils";
import ClipboardJS from "clipboard";
import i18next from "i18next";

type KVSecretViewDataProps = DefaultPageProps & { data: Map<string, unknown> };

export class KVSecretCodeVew extends Component<KVSecretViewDataProps, { syntax: string }> {
  syntaxSelectRef = createRef<HTMLSelectElement>();
  render() {
    const syntax = this.state.syntax || this.props.settings.kvViewDefaultLanguage;
    const secretData = Object.fromEntries(this.props.data);
    const codeData = dumpData(secretData, this.props.settings.kvViewIndent, syntax);
    return (
      <>
        <InputWithTitle title={i18next.t("kv_secret_syntax")}>
          <Select
            selectRef={this.syntaxSelectRef}
            onChange={() => {
              this.setState({ syntax: this.syntaxSelectRef.current.value });
            }}
          >
            {SupportedLanguages.map((lang) => {
              return (
                <SelectOption
                  name={lang.readable}
                  value={lang.name}
                  selected={this.props.settings.kvViewDefaultLanguage == lang.name}
                />
              );
            })}
          </Select>
        </InputWithTitle>
        <CodeBlock language={toPrismCode(syntax)} code={codeData} />
      </>
    );
  }
}

class CopyTOTPButton extends Component<
  { baseMount: string; totpKey: string; api: API },
  { code: string }
> {
  buttonRef = createRef<HTMLButtonElement>();

  timer: unknown;

  updateTOTPCode(): void {
    void this.props.api.getTOTPCode(this.props.baseMount, this.props.totpKey).then((code) => {
      this.setState({ code });
    });
  }

  componentDidMount(): void {
    this.updateTOTPCode();
    this.timer = setInterval(() => {
      this.updateTOTPCode();
    }, 3000);

    const clipboard = new ClipboardJS(this.buttonRef.current);
    addClipboardNotifications(clipboard, 600);
  }

  componentWillUnmount(): void {
    clearInterval(this.timer as number);
  }

  render(): JSX.Element {
    return (
      <Button
        buttonRef={this.buttonRef}
        text={i18next.t("kv_secret_copy_totp_btn")}
        color="primary"
        data-clipboard-text={this.state.code}
      />
    );
  }
}

export class KVSecretNormalVew extends Component<KVSecretViewDataProps> {
  render() {
    return (
      <div>
        {Array.from(this.props.data).map((data: [string, unknown]) => {
          const key = data[0];
          const value = data[1] as string;

          if (key == "__vaultui_totp_path") return <></>;

          return (
            <Grid size={GridSizes.NORMAL}>
              <CopyableInputBox text={key} copyable />
              <CopyableInputBox
                text={value}
                hideLikePassword={this.props.settings.kvHideKeyValues.includes(key)}
                copyable
              />
            </Grid>
          );
        })}
      </div>
    );
  }
}

export type KVSecretViewProps = DefaultPageProps & {
  kvData: Record<string, unknown>;
};

function KVTOTPExtra(props: KVSecretViewDataProps) {
  if (props.data.has("__vaultui_totp_path")) {
    const value = props.data.get("__vaultui_totp_path") as string;
    const baseMount = value.split("/")[0];
    const totpKey = value.split("/")[1];

    return (
      <p>
        <CopyTOTPButton api={props.api} baseMount={baseMount} totpKey={totpKey} />
        <Button
          text={i18next.t("kv_secret_view_totp_btn")}
          color="secondary"
          route={totpListURL(baseMount, totpKey)}
        />
      </p>
    );
  } else {
    return <></>;
  }
}

export class KVSecretVew extends Component<KVSecretViewProps, { syntax: string }> {
  render(): JSX.Element {
    const secretsMap = sortedObjectMap(this.props.kvData);
    let isMultiLevel = false;

    for (const value of secretsMap.values()) {
      if (typeof value == "object") isMultiLevel = true;
    }

    let showableAsHybrid = false;
    for (const value of secretsMap.values()) {
      if (typeof value != "object") showableAsHybrid = true;
    }

    if (showableAsHybrid && this.props.settings.kvUseHybridView && secretsMap.size >= 1) {
      let kvNormalViewMap = new Map();
      let kvCodeViewMap = new Map();

      for (const key of secretsMap.keys()) {
        const value = secretsMap.get(key);
        if (typeof value == "object") {
          kvCodeViewMap = kvCodeViewMap.set(key, value);
        } else {
          kvNormalViewMap = kvNormalViewMap.set(key, value);
        }
      }

      kvNormalViewMap = sortedObjectMap(
        Object.fromEntries(kvNormalViewMap) as Record<string, unknown>,
      );
      kvCodeViewMap = sortedObjectMap(Object.fromEntries(kvCodeViewMap) as Record<string, unknown>);

      return (
        <div>
          <KVSecretNormalVew {...this.props} data={kvNormalViewMap} />
          {kvCodeViewMap.size >= 1 && <KVSecretCodeVew {...this.props} data={kvCodeViewMap} />}
          <KVTOTPExtra {...this.props} data={secretsMap} />
        </div>
      );
    }

    if (isMultiLevel || this.props.settings.kvAlwaysCodeView) {
      return (
        <div>
          <KVSecretCodeVew {...this.props} data={secretsMap} />
          <KVTOTPExtra {...this.props} data={secretsMap} />
        </div>
      );
    } else {
      return (
        <div>
          <KVSecretNormalVew {...this.props} data={secretsMap} />
          <KVTOTPExtra {...this.props} data={secretsMap} />
        </div>
      );
    }
  }
}

type KeyValueViewState = {
  baseMount: string;
  secretPath: string[];
  secretItem: string;
  caps: string[];
  secretInfo: Record<string, unknown>;
  kvVersion: string;
  isDeleted: boolean;
  secretVersion: string;
};

export class KeyValueView extends Component<DefaultPageProps, KeyValueViewState> {
  async componentDidMount() {
    try {
      await this.getKVViewData();
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  async getKVViewData() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = splitKVPath(this.props.matches["secretPath"]);
    const secretItem = this.props.matches["item"];
    const secretVersion = this.props.matches["version"];

    const caps = (await this.props.api.getCapabilities(baseMount, secretPath, secretItem))
      .capabilities;

    const mountInfo = await this.props.api.getMount(baseMount);
    const kvVersion = mountInfo.options.version;

    let secretInfo: Record<string, unknown>;

    if (kvVersion == "2") {
      try {
        secretInfo = await this.props.api.getSecretKV2(
          baseMount,
          secretPath,
          secretItem,
          secretVersion,
        );
      } catch (e) {
        if (e == DoesNotExistError) {
          secretInfo = null;
        } else {
          throw e;
        }
      }
    } else {
      secretInfo = await this.props.api.getSecretKV1(baseMount, secretPath, secretItem);
    }

    const isDeleted = secretInfo == null && kvVersion == "2";

    this.setState({
      baseMount,
      secretPath,
      secretItem,
      caps,
      secretInfo,
      kvVersion,
      isDeleted,
      secretVersion,
    });
  }
  render() {
    if (!this.state.secretInfo) return;

    // Delete Secret on kv-v1
    let deleteButtonText = i18next.t("kv_secret_delete_btn");
    if (this.state.kvVersion == "2" && this.state.secretVersion == "null") {
      // Delete All
      deleteButtonText = i18next.t("kv_secret_delete_all_btn");
    } else if (this.state.kvVersion == "2" && this.state.secretVersion != "null") {
      // Delete Version X
      deleteButtonText = i18next.t("kv_secret_delete_version_btn", {
        version: this.state.secretVersion,
      });
    }

    return (
      <>
        <SecretTitleElement
          type="kv"
          item={this.props.matches["item"]}
          baseMount={this.state.baseMount}
          secretPath={this.state.secretPath}
        />
        <div>
          <p id="buttonsBlock">
            {
              // Delete Button
              !this.state.isDeleted && this.state.caps.includes("delete") && (
                <Button
                  text={deleteButtonText}
                  color="danger"
                  route={kvDeleteURL(
                    this.state.baseMount,
                    this.state.secretPath,
                    this.state.secretItem,
                    this.state.secretVersion,
                  )}
                />
              )
            }
            {this.state.secretVersion == "null" && this.state.caps.includes("update") && (
              <Button
                text={i18next.t("common_edit")}
                color="primary"
                route={kvEditURL(
                  this.state.baseMount,
                  this.state.secretPath,
                  this.state.secretItem,
                )}
              />
            )}
            {!this.state.isDeleted && this.state.kvVersion == "2" && (
              <Button
                text={i18next.t("kv_secret_versions_btn")}
                color="secondary"
                route={kvVersionsURL(
                  this.state.baseMount,
                  this.state.secretPath,
                  this.state.secretItem,
                )}
              />
            )}
          </p>

          {!this.state.isDeleted && <KVSecretVew {...this.props} kvData={this.state.secretInfo} />}

          {this.state.isDeleted && (
            <>
              <p>{i18next.t("kv_secret_deleted_text")}</p>
              <Button
                text={i18next.t("kv_secret_restore_btn")}
                color="primary"
                onClick={async () => {
                  await this.props.api.undeleteSecret(
                    this.state.baseMount,
                    this.state.secretPath,
                    this.state.secretItem,
                    this.state.secretVersion,
                  );
                  // TODO: Is there a better way to force refresh of a Component?
                  this.setState({});
                  await this.componentDidMount();
                }}
              />
            </>
          )}
        </div>
      </>
    );
  }
}
