import { Button } from "../../elements/Button";
import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { InlineButtonBox } from "../../elements/InlineButtonBox";
import { MountType } from "../../../api/types/mount";
import { PageTitle } from "../../elements/PageTitle";
import { kvListURL, totpListURL, transitListURL } from "../pageLinks";
import { sendErrorNotification } from "../../elements/ErrorMessage";
import { sortedObjectMap } from "../../../utils";
import i18next from "i18next";

const supportedMountTypes = ["kv", "totp", "transit", "cubbyhole"];

export function isSupportedMount(mount: MountType): boolean {
  if (typeof mount != "object") return false;
  if (mount == null) return false;
  if (!("type" in mount)) return false;
  if (!supportedMountTypes.includes(mount.type)) return false;
  return true;
}

export type MountLinkProps = {
  mount: MountType;
  baseMount: string;
};

function MountLink(props: MountLinkProps): JSX.Element {
  const mount = props.mount;
  const baseMount = props.baseMount;

  let linkText = "";
  let link = "";
  if (mount.type == "kv") {
    linkText = `K/V (v${mount.options.version}) - ${baseMount}`;
    link = kvListURL(baseMount, []);
  } else if (mount.type == "cubbyhole") {
    linkText = `Cubbyhole - ${baseMount}`;
    link = kvListURL(baseMount, []);
  } else if (mount.type == "totp") {
    linkText = `TOTP - ${baseMount}`;
    link = totpListURL(baseMount);
  } else if (mount.type == "transit") {
    linkText = `Transit - ${baseMount}`;
    link = transitListURL(baseMount);
  }

  return (
    <li>
      <a href={link}>{linkText}</a>
    </li>
  );
}

type SecretsState = {
  mountsMap: Map<string, MountType>;
  capabilities: string[];
};

export class Secrets extends Component<DefaultPageProps, SecretsState> {
  async componentDidMount() {
    try {
      const mountsCapabilities = await this.props.api.getCapsPath("/sys/mounts");
      const mounts = await this.props.api.getMounts();
      // sort it by secretPath so it's in alphabetical order consistantly.
      const mountsMap = sortedObjectMap(mounts);
      this.setState({
        capabilities: mountsCapabilities,
        mountsMap: mountsMap as Map<string, MountType>,
      });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    return (
      this.state.mountsMap && (
        <>
          <PageTitle title={i18next.t("secrets_home_page_title")} />
          <div>
            <InlineButtonBox>
              {this.state.capabilities.includes("sudo") &&
                this.state.capabilities.includes("create") && (
                  <Button
                    text={i18next.t("secrets_home_new_secrets_engine_button")}
                    color="primary"
                    route={"/secrets/new_secrets_engine"}
                  />
                )}
            </InlineButtonBox>
            <div class="uk-margin-top">
              <ul class="uk-nav">
                {Array.from(this.state.mountsMap).map((args: [string, MountType]) => {
                  const baseMount = args[0];
                  const mount = args[1];
                  if (isSupportedMount(mount)) {
                    return <MountLink mount={mount} baseMount={baseMount} />;
                  }
                })}
              </ul>
            </div>
          </div>
        </>
      )
    );
  }
}
