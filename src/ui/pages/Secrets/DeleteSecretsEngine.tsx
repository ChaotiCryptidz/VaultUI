import { Button } from "../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { ErrorMessage } from "../../elements/ErrorMessage";
import { Form } from "../../elements/forms/Form";
import { Margin } from "../../elements/Margin";
import { MarginInline } from "../../elements/MarginInline";
import { PageTitle } from "../../elements/PageTitle";
import { route } from "preact-router";
import i18next from "i18next";

export class DeleteSecretsEngine extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  render() {
    return (
      <>
        <PageTitle
          title={i18next.t("delete_secrets_engine_title", { mount: this.props.matches["mount"] })}
        />
        <Form
          onSubmit={async () => {
            await this.onSubmit();
          }}
        >
          <p>{i18next.t("delete_secrets_engine_message")}</p>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <MarginInline>
            <Button text={i18next.t("common_delete")} color="danger" type="submit" />
          </MarginInline>
        </Form>
      </>
    );
  }

  async onSubmit(): Promise<void> {
    try {
      await this.props.api.deleteMount(this.props.matches["mount"]);
      route("/secrets");
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }
}
