import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { Grid, GridSizes } from "../../elements/Grid";
import { PageTitle } from "../../elements/PageTitle";
import { Tile } from "../../elements/Tile";
import i18next from "i18next";

export class NewSecretsEngine extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title={i18next.t("new_secrets_engine_title")} />
        <Grid size={GridSizes.MATCHING_TWO_ROWS}>
          <Tile
            title={i18next.t("new_secrets_engine_kv_title")}
            description={i18next.t("new_secrets_engine_kv_description")}
            href="/secrets/new_secrets_engine/kv"
          />
          <Tile
            title={i18next.t("new_secrets_engine_totp_title")}
            description={i18next.t("new_secrets_engine_totp_description")}
            href="/secrets/new_secrets_engine/totp"
          />
          <Tile
            title={i18next.t("new_secrets_engine_transit_title")}
            description={i18next.t("new_secrets_engine_transit_description")}
            href="/secrets/new_secrets_engine/transit"
          />
        </Grid>
      </>
    );
  }
}
