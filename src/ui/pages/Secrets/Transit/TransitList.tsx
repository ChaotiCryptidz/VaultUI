import { Button } from "../../../elements/Button";
import { CapabilitiesType } from "../../../../api/types/capabilities";
import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { InlineButtonBox } from "../../../elements/InlineButtonBox";
import { SecretTitleElement } from "../SecretTitleElement";
import { delSecretsEngineURL, transitNewSecretURL, transitViewSecretURL } from "../../pageLinks";
import { sendErrorNotification } from "../../../elements/ErrorMessage";
import i18next from "i18next";

type TransitViewListState = {
  contentLoaded: boolean;
  transitKeysList: string[];
};

export class TransitViewListItem extends Component<
  { baseMount: string } & DefaultPageProps,
  TransitViewListState
> {
  constructor() {
    super();
    this.state = {
      contentLoaded: false,
      transitKeysList: [],
    };
  }

  timer: unknown;

  async getTransitKeys() {
    const transitKeys = await this.props.api.getTransitKeys(this.props.baseMount);
    this.setState({
      contentLoaded: true,
      transitKeysList: transitKeys,
    });
  }

  async componentDidMount() {
    try {
      await this.getTransitKeys();
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render(): JSX.Element {
    if (!this.state.contentLoaded) {
      return <p>{i18next.t("content_loading")}</p>;
    }

    if (this.state.transitKeysList.length == 0) {
      return <p>{i18next.t("transit_view_none_here_text")}</p>;
    }

    return (
      <ul class="uk-nav uk-nav-default">
        {...this.state.transitKeysList.map((key) => (
          <li>
            <a href={transitViewSecretURL(this.props.baseMount, key)}>{key}</a>
          </li>
        ))}
      </ul>
    );
  }
}

export class TransitList extends Component<DefaultPageProps, { caps: CapabilitiesType }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const mountsPath = "/sys/mounts/" + baseMount;

    try {
      const caps = await this.props.api.getCapabilitiesPath([mountsPath, baseMount]);
      this.setState({ caps });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    if (!this.state.caps) return;
    const baseMount = this.props.matches["baseMount"];
    const mountsPath = "/sys/mounts/" + baseMount;
    const mountCaps = this.state.caps[mountsPath];
    const transitCaps = this.state.caps[baseMount];

    return (
      <>
        <SecretTitleElement type="transit" baseMount={baseMount} />

        <InlineButtonBox>
          {transitCaps.includes("create") && (
            <Button
              text={i18next.t("common_new")}
              color="primary"
              route={transitNewSecretURL(baseMount)}
            />
          )}
          {mountCaps.includes("delete") && (
            <Button
              text={i18next.t("common_delete")}
              color="danger"
              route={delSecretsEngineURL(baseMount)}
            />
          )}
        </InlineButtonBox>
        <TransitViewListItem
          settings={this.props.settings}
          api={this.props.api}
          baseMount={baseMount}
        />
      </>
    );
  }
}
