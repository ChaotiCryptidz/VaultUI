import { Button } from "../../../elements/Button";
import { Checkbox } from "../../../elements/forms/Checkbox";
import { Component, createRef } from "preact";
import { CopyableBox } from "../../../elements/CopyableBox";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../elements/ErrorMessage";
import { FileUploadInput } from "../../../elements/FileUploadInput";
import { Form } from "../../../elements/forms/Form";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { TextArea } from "../../../elements/forms/TextArea";
import { fileToBase64 } from "../../../../utils/fileToBase64";
import i18next from "i18next";

export class TransitEncrypt extends Component<DefaultPageProps, { ciphertext: string }> {
  errorMessageRef = createRef<ErrorMessage>();

  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];

    const title = (
      <SecretTitleElement
        type="transit"
        baseMount={baseMount}
        item={secretItem}
        suffix={i18next.t("transit_encrypt_suffix")}
      />
    );

    if (!this.state.ciphertext) {
      return (
        <>
          {title}
          <Form onSubmit={async (data) => await this.onSubmit(data)}>
            <Margin>
              <TextArea
                name="plaintext"
                placeholder={i18next.t("transit_encrypt_input_placeholder")}
              />
            </Margin>

            <Margin>
              <FileUploadInput name="plaintext_file" />
            </Margin>

            <InputWithTitle title={i18next.t("transit_encrypt_already_encoded_checkbox")}>
              <Checkbox name="base64Checkbox" />
            </InputWithTitle>

            <Margin>
              <ErrorMessage ref={this.errorMessageRef} />
            </Margin>

            <Button text={i18next.t("transit_encrypt")} color="primary" type="submit" />
          </Form>
        </>
      );
    } else {
      return (
        <>
          {title}
          <CopyableBox
            title={i18next.t("transit_encrypt_encryption_result_title")}
            contentString={this.state.ciphertext}
            goBack={() => {
              this.setState({ ciphertext: null });
            }}
          />
        </>
      );
    }
  }

  async onSubmit(data: FormData): Promise<void> {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];

    const base64Checkbox = data.get("base64Checkbox") as string;

    let plaintext = data.get("plaintext") as string;

    const plaintext_file = data.get("plaintext_file") as File;
    if (plaintext_file.size > 0) {
      plaintext = (await fileToBase64(plaintext_file)).replace("data:text/plain;base64,", "");
      plaintext = base64Checkbox == "on" ? atob(plaintext) : plaintext;
    } else {
      plaintext = base64Checkbox == "on" ? plaintext : btoa(plaintext);
    }

    try {
      const res = await this.props.api.transitEncrypt(baseMount, secretItem, {
        plaintext: plaintext,
      });
      this.setState({ ciphertext: res.ciphertext });
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }
}
