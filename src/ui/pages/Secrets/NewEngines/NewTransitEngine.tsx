import { Button } from "../../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../elements/ErrorMessage";
import { Form } from "../../../elements/forms/Form";
import { Margin } from "../../../elements/Margin";
import { MarginInline } from "../../../elements/MarginInline";
import { PageTitle } from "../../../elements/PageTitle";
import { TextInput } from "../../../elements/forms/TextInput";
import { route } from "preact-router";
import i18next from "i18next";

export class NewTransitEngine extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  render() {
    return (
      <>
        <PageTitle title={i18next.t("new_transit_engine_title")} />
        <Form onSubmit={(data) => this.submit(data)}>
          <Margin>
            <TextInput name="name" placeholder={i18next.t("common_name")} required />
          </Margin>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <MarginInline>
            <Button text={i18next.t("common_create")} color="primary" type="submit" />
          </MarginInline>
        </Form>
        ,
      </>
    );
  }

  async submit(data: FormData): Promise<void> {
    const name = data.get("name") as string;

    try {
      await this.props.api.newMount({
        name: name,
        type: "transit",
      });
      route("/secrets/transit/list/" + name + "/");
    } catch (e) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }
}
