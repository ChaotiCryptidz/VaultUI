import { Button } from "../../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../elements/ErrorMessage";
import { Form } from "../../../elements/forms/Form";
import { Margin } from "../../../elements/Margin";
import { MarginInline } from "../../../elements/MarginInline";
import { PageTitle } from "../../../elements/PageTitle";
import { Select, SelectOption } from "../../../elements/forms/Select";
import { TextInput } from "../../../elements/forms/TextInput";
import { kvListURL } from "../../pageLinks";
import { route } from "preact-router";
import i18next from "i18next";

export class NewKVEngine extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  render() {
    return (
      <>
        <PageTitle title={i18next.t("new_kv_engine_title")} />
        <Form onSubmit={(data) => this.submit(data)}>
          <Margin>
            <TextInput name="name" placeholder={i18next.t("common_name")} required />
          </Margin>
          <Margin>
            <Select name="version">
              <SelectOption name={i18next.t("new_kv_engine_version_2")} value="2" />
              <SelectOption name={i18next.t("new_kv_engine_version_1")} value="1" />
            </Select>
          </Margin>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <MarginInline>
            <Button text={i18next.t("common_create")} color="primary" type="submit" />
          </MarginInline>
        </Form>
      </>
    );
  }

  async submit(data: FormData): Promise<void> {
    const name = data.get("name") as string;
    const version = data.get("version") as string;

    try {
      await this.props.api.newMount({
        name: name,
        type: "kv",
        options: {
          version: version,
        },
      });
      route(kvListURL(name, []));
    } catch (e) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }
}
