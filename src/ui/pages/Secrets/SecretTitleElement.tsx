import { JSX } from "preact/jsx-runtime";
import { kvListURL, kvViewURL, transitViewSecretURL } from "../pageLinks";

type SecretTitleElementProps = {
  type: string;
  baseMount: string;
  secretPath?: string[];
  item?: string;
  suffix?: string;
};

export function SecretTitleElement(props: SecretTitleElementProps): JSX.Element {
  const type = props.type;
  const item = props.item || "";
  const suffix = props.suffix || "";
  const baseMount = props.baseMount;
  const secretPath = props.secretPath || [];

  return (
    <h3 class="uk-card-title" id="pageTitle">
      <div>
        <a test-data="secrets-title-first-slash" href="/secrets">
          {"/ "}
        </a>

        <a href={"/secrets/" + type + "/list/" + baseMount} test-data="secrets-title-baseMount">
          {baseMount + "/ "}
        </a>

        {...secretPath.map((secretPath, index, secretPaths) => {
          // Sometimes we pass a extra / in urls
          // just ignore this.
          if (secretPath.length < 1) return;
          if (type != "kv") return;

          return (
            <a
              test-data="secrets-title-secretPath"
              href={kvListURL(baseMount, secretPaths.slice(0, index + 1))}
            >
              {secretPath + "/" + " "}
            </a>
          );
        })}

        {item.length != 0 &&
          (() => {
            if (!["kv", "transit"].includes(type)) {
              return <span>{item}</span>;
            }

            let link = "";
            if (type == "kv") {
              link = kvViewURL(baseMount, secretPath, item);
            } else if (type == "transit") {
              link = transitViewSecretURL(baseMount, item);
            }

            return <a href={link}>{item}</a>;
          })()}

        {suffix.length != 0 && <span>{suffix}</span>}
      </div>
    </h3>
  );
}
