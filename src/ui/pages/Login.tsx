import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { PageTitle } from "../elements/PageTitle";
import { TokenLoginForm } from "./Login_Token";
import { UsernameLoginForm } from "./Login_Username";
import i18next from "i18next";

export class Login extends Component<DefaultPageProps> {
  render(): JSX.Element {
    return (
      <>
        <PageTitle title={i18next.t("log_in_title")} />
        <div>
          <ul class="uk-subnav uk-subnav-pill" uk-switcher=".switcher-container">
            <li>
              <a>{i18next.t("log_in_with_token")}</a>
            </li>
            <li>
              <a>{i18next.t("log_in_with_username")}</a>
            </li>
          </ul>

          <ul class="uk-switcher uk-margin switcher-container">
            <li>
              <TokenLoginForm {...this.props} />
            </li>
            <li>
              <UsernameLoginForm {...this.props} />
            </li>
          </ul>
        </div>
      </>
    );
  }
}
