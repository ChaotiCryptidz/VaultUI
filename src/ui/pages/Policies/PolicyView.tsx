import { Button } from "../../elements/Button";
import { CodeBlock } from "../../elements/CodeBlock";
import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { InlineButtonBox } from "../../elements/InlineButtonBox";
import { Margin } from "../../elements/Margin";
import { PageTitle } from "../../elements/PageTitle";
import { policyDeleteURL, policyEditURL } from "../pageLinks";
import { sendErrorNotification } from "../../elements/ErrorMessage";
import i18next from "i18next";

export class PolicyView extends Component<
  DefaultPageProps,
  { policy: string; policyName: string }
> {
  async componentDidMount() {
    const policyName = this.props.matches["policyName"];
    try {
      const policy = await this.props.api.getPolicy(policyName);
      this.setState({
        policy,
        policyName,
      });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    if (!this.state.policy) return;
    return (
      <>
        <PageTitle title={i18next.t("policy_view_title", { policy: this.state.policyName })} />
        <div>
          <InlineButtonBox>
            <Button
              text={i18next.t("common_edit")}
              color="primary"
              route={policyEditURL(this.state.policyName)}
            />
            {this.state.policyName !== "default" && (
              <Button
                text={i18next.t("common_delete")}
                color="danger"
                route={policyDeleteURL(this.state.policyName)}
              />
            )}
          </InlineButtonBox>

          <Margin>
            <CodeBlock language="hcl" code={this.state.policy} />
          </Margin>
        </div>
      </>
    );
  }
}
