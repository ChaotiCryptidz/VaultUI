import { Button } from "../../elements/Button";
import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { InlineButtonBox } from "../../elements/InlineButtonBox";
import { Margin } from "../../elements/Margin";
import { PageTitle } from "../../elements/PageTitle";
import { policyNewURL, policyViewURL } from "../pageLinks";
import { sendErrorNotification } from "../../elements/ErrorMessage";
import i18next from "i18next";

export class PoliciesHome extends Component<DefaultPageProps, { policies: string[] }> {
  async componentDidMount() {
    try {
      let policies = await this.props.api.getPolicies();
      policies = policies.sort();
      policies = policies.filter(function (policy_name) {
        return policy_name !== "root";
      });
      this.setState({
        policies: policies,
      });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    if (!this.state.policies) return;

    return (
      <>
        <PageTitle title={i18next.t("policies_home_title")} />
        <div>
          <InlineButtonBox>
            <Button text={i18next.t("common_new")} color="primary" route={policyNewURL()} />
          </InlineButtonBox>

          <Margin>
            <ul class="uk-nav uk-nav-default">
              {this.state.policies.map((policyName: string) => (
                <li>
                  <a href={policyViewURL(policyName)}>{policyName}</a>
                </li>
              ))}
            </ul>
          </Margin>
        </div>
      </>
    );
  }
}
