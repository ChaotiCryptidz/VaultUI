import { Button } from "../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { ErrorMessage } from "../../elements/ErrorMessage";
import { Margin } from "../../elements/Margin";
import { PageTitle } from "../../elements/PageTitle";
import { route } from "preact-router";
import i18next from "i18next";

export class PolicyDelete extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  async onDelete() {
    const policyName = this.props.matches["policyName"];
    try {
      await this.props.api.deletePolicy(policyName);
      route("/policies");
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }

  render() {
    const policyName = this.props.matches["policyName"];
    return (
      <>
        <PageTitle title={i18next.t("policy_delete_title", { policy: policyName })} />
        <div>
          <h5>{i18next.t("policy_delete_text")}</h5>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <Button
            text={i18next.t("common_delete")}
            color="danger"
            onClick={async () => await this.onDelete()}
          />
        </div>
      </>
    );
  }
}
