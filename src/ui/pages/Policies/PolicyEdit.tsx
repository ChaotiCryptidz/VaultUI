import { Button } from "../../elements/Button";
import { CodeEditor } from "../../elements/CodeEditor";
import { Component, JSX, createRef } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { ErrorMessage, sendErrorNotification } from "../../elements/ErrorMessage";
import { Margin } from "../../elements/Margin";
import { MarginInline } from "../../elements/MarginInline";
import { PageTitle } from "../../elements/PageTitle";
import { policyViewURL } from "../pageLinks";
import { route } from "preact-router";
import i18next from "i18next";

type PolicyEditorProps = DefaultPageProps & {
  policyName: string;
};

type PolicyEditorStateUnloaded = {
  dataLoaded: false;
};

type PolicyEditorStateLoaded = {
  dataLoaded: true;
  policyData: string;
  code: string;
};

type PolicyEditorState = PolicyEditorStateUnloaded | PolicyEditorStateLoaded;

export class PolicyEditor extends Component<PolicyEditorProps, PolicyEditorState> {
  constructor() {
    super();
    this.state = {
      dataLoaded: false,
    };
  }

  errorMessageRef = createRef<ErrorMessage>();

  async componentDidMount() {
    if (!this.state.dataLoaded) {
      try {
        await this.loadData();
      } catch (e: unknown) {
        const error = e as Error;
        sendErrorNotification(error.message);
      }
    }
  }

  async editorSave(): Promise<void> {
    if (!this.state.dataLoaded) return;

    try {
      await this.props.api.createOrUpdatePolicy(this.props.policyName, this.state.code);
      route(policyViewURL(this.props.policyName));
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }

  async loadData(): Promise<void> {
    const policyData = await this.props.api.getPolicy(this.props.policyName);
    this.setState({
      dataLoaded: true,
      policyData: policyData,
      code: policyData,
    });
    return;
  }

  render(): JSX.Element {
    if (!this.state.dataLoaded) {
      return <p>{i18next.t("content_loading")}</p>;
    }

    return (
      <div>
        <Margin>
          <ErrorMessage ref={this.errorMessageRef} />
        </Margin>

        <Margin>
          <CodeEditor
            language="hcl"
            tabSize={2}
            code={this.state.policyData}
            onUpdate={(code) => {
              this.setState({ code: code });
            }}
          />
        </Margin>
        <MarginInline>
          <Button
            text={i18next.t("common_edit")}
            color="primary"
            onClick={() => this.editorSave()}
          />
        </MarginInline>
      </div>
    );
  }
}

export class PolicyEdit extends Component<DefaultPageProps> {
  render() {
    const policyName = this.props.matches["policyName"];
    return (
      <>
        <PageTitle title={i18next.t("policy_edit_title", { policy: policyName })} />
        <div>
          <PolicyEditor {...this.props} policyName={policyName} />
        </div>
      </>
    );
  }
}
