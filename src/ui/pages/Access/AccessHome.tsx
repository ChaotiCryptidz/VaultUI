import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { Grid, GridSizes } from "../../elements/Grid";
import { PageTitle } from "../../elements/PageTitle";
import { Tile } from "../../elements/Tile";
import i18next from "i18next";

export class AccessHomePage extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title={i18next.t("access_home_page_title")} />
        <Grid size={GridSizes.MATCHING_TWO_ROWS}>
          <Tile
            title={i18next.t("access_auth_methods_title")}
            description={i18next.t("access_auth_methods_description")}
            icon="sign-in"
            href="/access/auth"
          />
          <Tile
            title={i18next.t("access_entities_title")}
            description={i18next.t("access_entities_description")}
            icon="user"
            notImplemented={true}
          />
          <Tile
            title={i18next.t("access_groups_title")}
            description={i18next.t("access_groups_description")}
            icon="users"
            notImplemented={true}
          />
          <Tile
            title={i18next.t("access_leases_title")}
            description={i18next.t("access_leases_description")}
            icon="unlock"
            notImplemented={true}
          />
        </Grid>
      </>
    );
  }
}
