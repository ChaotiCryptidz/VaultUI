import { AuthMethod } from "../../../../api/types/auth";
import { Button } from "../../../elements/Button";
import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { PageTitle } from "../../../elements/PageTitle";
import { authViewConfigURL, userPassUserListURL } from "../../pageLinks";
import { notImplementedNotification, sendErrorNotification } from "../../../elements/ErrorMessage";
import { objectToMap } from "../../../../utils";
import i18next from "i18next";

export type AuthListElementProps = {
  path: string;
  method: AuthMethod;
};

export function AuthListElement(props: AuthListElementProps): JSX.Element {
  const isViewable = ["userpass"].includes(props.method.type);

  let link = "";
  if (props.method.type == "userpass") {
    link = userPassUserListURL(props.path);
  }

  return (
    <div class="uk-padding-small uk-background-secondary">
      <span class="uk-h4 uk-margin-bottom">{props.path}</span>
      <span class="uk-text-muted">{` (${props.method.accessor})`}</span>
      <div class="uk-margin-top">
        {isViewable && <Button text={i18next.t("common_view")} color="primary" route={link} />}
        <Button
          text={i18next.t("auth_home_view_config")}
          color="primary"
          route={authViewConfigURL(props.path)}
        />

        <Button
          text={i18next.t("auth_home_edit_config")}
          color="primary"
          onClick={notImplementedNotification}
        />
      </div>
    </div>
  );
}

export class AuthHome extends Component<DefaultPageProps, { authList: Map<string, AuthMethod> }> {
  async componentDidMount() {
    try {
      const authList = objectToMap(await this.props.api.listAuth()) as Map<string, AuthMethod>;
      this.setState({ authList });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }
  render() {
    if (!this.state.authList) return;

    return (
      <>
        <PageTitle title={i18next.t("auth_home_title")} />
        <div>
          {Array.from(this.state.authList).map((values: [string, AuthMethod]) => (
            <AuthListElement path={values[0]} method={values[1]} />
          ))}
        </div>
      </>
    );
  }
}
