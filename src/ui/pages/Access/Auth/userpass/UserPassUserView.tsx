import { Button } from "../../../../elements/Button";
import { Component } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { HeaderAndContent } from "../../../../elements/HeaderAndContent";
import { InlineButtonBox } from "../../../../elements/InlineButtonBox";
import { Margin } from "../../../../elements/Margin";
import { PageTitle } from "../../../../elements/PageTitle";
import { UserType } from "../../../../../api/types/user";
import { sendErrorNotification } from "../../../../elements/ErrorMessage";
import { toStr } from "../../../../../utils";
import { userPassUserDeleteURL, userPassUserEditURL } from "../../../pageLinks";
import i18next from "i18next";

export class UserPassUserView extends Component<DefaultPageProps, { user_data: UserType }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    try {
      const user_data = await this.props.api.getUserPassUser(baseMount, user);
      this.setState({ user_data });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    if (!this.state.user_data) return;
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    const user_data = this.state.user_data;

    return (
      <>
        <PageTitle title={i18next.t("userpass_user_view_title")} />

        <div>
          <InlineButtonBox>
            <Button
              text={i18next.t("common_delete")}
              color="danger"
              route={userPassUserDeleteURL(baseMount, user)}
            />
            <Button
              text={i18next.t("common_edit")}
              color="primary"
              route={userPassUserEditURL(baseMount, user)}
            />
          </InlineButtonBox>

          <br />

          <Margin>
            <h4>{i18next.t("auth_common_token_config")}</h4>
          </Margin>

          <table class="uk-table">
            <tbody>
              <HeaderAndContent
                title={i18next.t("common_cidrs")}
                content={user_data.token_bound_cidrs.join()}
              />
              <HeaderAndContent
                title={i18next.t("common_exp_max_ttl")}
                content={toStr(user_data.token_explicit_max_ttl)}
              />
              <HeaderAndContent
                title={i18next.t("common_max_ttl")}
                content={toStr(user_data.token_max_ttl)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_default_policy_attached")}
                content={toStr(user_data.token_no_default_policy)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_max_token_uses")}
                content={toStr(user_data.token_num_uses)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_token_peroid")}
                content={toStr(user_data.token_period)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_policies")}
                content={user_data.token_policies.join()}
              />
              <HeaderAndContent
                title={i18next.t("common_initial_ttl")}
                content={toStr(user_data.token_ttl)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_type")}
                content={toStr(user_data.token_type)}
              />
            </tbody>
          </table>
        </div>
      </>
    );
  }
}
