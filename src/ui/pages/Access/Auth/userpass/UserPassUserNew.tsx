import { Button } from "../../../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../../elements/ErrorMessage";
import { Form } from "../../../../elements/forms/Form";
import { InputWithTitle } from "../../../../elements/InputWithTitle";
import { Margin } from "../../../../elements/Margin";
import { MarginInline } from "../../../../elements/MarginInline";
import { PageTitle } from "../../../../elements/PageTitle";
import { PasswordInput } from "../../../../elements/forms/PasswordInput";
import { TextInput } from "../../../../elements/forms/TextInput";
import { UserType } from "../../../../../api/types/user";
import { route } from "preact-router";
import { userPassUserViewURL } from "../../../pageLinks";
import i18next from "i18next";

export class UserPassUserNew extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  render() {
    return (
      <>
        <PageTitle title={i18next.t("userpass_user_new_title")} />
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <Margin>
            <InputWithTitle title={i18next.t("common_username")}>
              <TextInput name="username" />
            </InputWithTitle>
          </Margin>

          <Margin>
            <InputWithTitle title={i18next.t("common_password")}>
              <PasswordInput name="password" />
            </InputWithTitle>
          </Margin>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <MarginInline>
            <Button text={i18next.t("common_create")} color="primary" type="submit" />
          </MarginInline>
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const baseMount = this.props.matches["baseMount"];
    const apiData: Partial<UserType> = {
      password: data.get("password") as string,
    };
    try {
      await this.props.api.createOrUpdateUserPassUser(
        baseMount,
        data.get("username") as string,
        apiData,
      );
      route(userPassUserViewURL(baseMount, data.get("username") as string));
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }
}
