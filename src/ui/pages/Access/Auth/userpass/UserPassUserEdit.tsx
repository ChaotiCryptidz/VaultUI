import { Button } from "../../../../elements/Button";
import { Checkbox } from "../../../../elements/forms/Checkbox";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { ErrorMessage, sendErrorNotification } from "../../../../elements/ErrorMessage";
import { Form } from "../../../../elements/forms/Form";
import { InputWithTitle } from "../../../../elements/InputWithTitle";
import { Margin } from "../../../../elements/Margin";
import { MarginInline } from "../../../../elements/MarginInline";
import { NumberInput } from "../../../../elements/forms/NumberInput";
import { PageTitle } from "../../../../elements/PageTitle";
import { PasswordInput } from "../../../../elements/forms/PasswordInput";
import { TextInput } from "../../../../elements/forms/TextInput";
import { UserType } from "../../../../../api/types/user";
import { route } from "preact-router";
import { toStr } from "../../../../../utils";
import { userPassUserViewURL } from "../../../pageLinks";
import i18next from "i18next";

const removeEmptyStrings = (arr: string[]) => arr.filter((e) => e.length > 0);

export class UserPassUserEdit extends Component<DefaultPageProps, { user_data: UserType }> {
  errorMessageRef = createRef<ErrorMessage>();

  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    try {
      const user_data = await this.props.api.getUserPassUser(baseMount, user);
      this.setState({ user_data });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    if (!this.state.user_data) return;
    const user_data = this.state.user_data;

    return (
      <>
        <PageTitle title={i18next.t("userpass_user_edit_title")} />
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <InputWithTitle title={i18next.t("common_password")}>
            <PasswordInput name="password" />
          </InputWithTitle>

          <br />

          <Margin>
            <h4>{i18next.t("auth_common_token_config")}</h4>
          </Margin>

          <InputWithTitle title={i18next.t("common_cidrs")}>
            <TextInput name="cidrs" value={user_data.token_bound_cidrs.join()} />
          </InputWithTitle>

          <InputWithTitle title={i18next.t("common_exp_max_ttl")}>
            <NumberInput name="exp_max_ttl" value={toStr(user_data.token_explicit_max_ttl)} />
          </InputWithTitle>

          <InputWithTitle title={i18next.t("common_max_ttl")}>
            <NumberInput name="max_ttl" value={toStr(user_data.token_max_ttl)} />
          </InputWithTitle>

          <InputWithTitle title={i18next.t("auth_common_default_policy_attached")}>
            <Checkbox name="def_pol_attached" checked={user_data.token_no_default_policy} />
          </InputWithTitle>

          <InputWithTitle title={i18next.t("auth_common_max_token_uses")}>
            <NumberInput name="max_uses" value={toStr(user_data.token_num_uses)} />
          </InputWithTitle>

          <InputWithTitle title={i18next.t("auth_common_token_peroid")}>
            <NumberInput name="period" value={toStr(user_data.token_period)} />
          </InputWithTitle>

          <InputWithTitle title={i18next.t("auth_common_policies")}>
            <TextInput name="policies" value={user_data.token_policies.join()} />
          </InputWithTitle>

          <InputWithTitle title={i18next.t("common_initial_ttl")}>
            <NumberInput name="initial_ttl" value={toStr(user_data.token_ttl)} />
          </InputWithTitle>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <MarginInline>
            <Button text={i18next.t("common_edit")} color="primary" type="submit" />
          </MarginInline>
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    const apiData: Partial<UserType> = {
      token_bound_cidrs: removeEmptyStrings(String(data.get("cidrs")).split(",")),
      token_explicit_max_ttl: parseInt(data.get("exp_max_ttl") as string, 10),
      token_max_ttl: parseInt(data.get("max_ttl") as string, 10),
      token_no_default_policy: (data.get("def_pol_attached") as string) == "true",
      token_num_uses: parseInt(data.get("max_uses") as string, 10),
      token_period: parseInt(data.get("period") as string, 10),
      token_policies: removeEmptyStrings(String(data.get("policies")).split(",")),
      token_ttl: parseInt(data.get("initial_ttl") as string, 10),
    };

    const password = data.get("password") as string;

    if (password.length > 0) {
      apiData.password = password;
    }

    try {
      await this.props.api.createOrUpdateUserPassUser(baseMount, user, apiData);
      route(userPassUserViewURL(baseMount, user));
    } catch (e: unknown) {
      const error = e as Error;
      this.errorMessageRef.current.setErrorMessage(error.message);
    }
  }
}
