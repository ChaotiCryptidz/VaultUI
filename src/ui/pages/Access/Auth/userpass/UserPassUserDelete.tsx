import { Button } from "../../../../elements/Button";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { ErrorMessage } from "../../../../elements/ErrorMessage";
import { Margin } from "../../../../elements/Margin";
import { PageTitle } from "../../../../elements/PageTitle";
import { route } from "preact-router";
import { userPassUserListURL } from "../../../pageLinks";
import i18next from "i18next";

export class UserPassUserDelete extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  render() {
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    return (
      <>
        <PageTitle title={i18next.t("userpass_user_delete_title")} />
        <div>
          <h5>{i18next.t("userpass_user_delete_text")}</h5>

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <Button
            text={i18next.t("common_delete")}
            color="danger"
            onClick={async () => {
              try {
                await this.props.api.deleteUserPassUser(baseMount, user);
                route(userPassUserListURL(baseMount));
              } catch (e: unknown) {
                const error = e as Error;
                this.errorMessageRef.current.setErrorMessage(error.message);
              }
            }}
          />
        </div>
      </>
    );
  }
}
