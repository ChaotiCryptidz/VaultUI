import { Button } from "../../../../elements/Button";
import { Component } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { Margin } from "../../../../elements/Margin";
import { PageTitle } from "../../../../elements/PageTitle";
import { sendErrorNotification } from "../../../../elements/ErrorMessage";
import { userPassUserNewURL, userPassUserViewURL } from "../../../pageLinks";
import i18next from "i18next";

export class UserPassUsersList extends Component<DefaultPageProps, { users: string[] }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];

    try {
      const users = await this.props.api.listUserPassUsers(baseMount);
      this.setState({ users });
    } catch (e: unknown) {
      const error = e as Error;
      sendErrorNotification(error.message);
    }
  }

  render() {
    if (!this.state.users) return;
    const baseMount = this.props.matches["baseMount"];

    return (
      <>
        <PageTitle title={i18next.t("userpass_users_list_title")} />
        <div>
          <Margin>
            <Button
              text={i18next.t("common_new")}
              color="primary"
              route={userPassUserNewURL(baseMount)}
            />
          </Margin>

          <ul>
            {...this.state.users.map((user) => (
              <li>
                <a href={userPassUserViewURL(baseMount, user)}>{user}</a>
              </li>
            ))}
          </ul>
        </div>
      </>
    );
  }
}
