import { Button } from "../elements/Button";
import { Component, JSX, createRef } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { ErrorMessage } from "../elements/ErrorMessage";
import { Form } from "../elements/forms/Form";
import { InputWithTitle } from "../elements/InputWithTitle";
import { Margin } from "../elements/Margin";
import { MarginInline } from "../elements/MarginInline";
import { PasswordInput } from "../elements/forms/PasswordInput";
import { route } from "preact-router";
import i18next from "i18next";

export class TokenLoginForm extends Component<DefaultPageProps> {
  errorMessageRef = createRef<ErrorMessage>();

  render(): JSX.Element {
    return (
      <Form onSubmit={(data) => this.onSubmit(data)}>
        <Margin>
          <InputWithTitle title={i18next.t("log_in_token_input")}>
            <PasswordInput name="token" required />
          </InputWithTitle>
        </Margin>

        <Margin>
          <ErrorMessage ref={this.errorMessageRef} />
        </Margin>

        <MarginInline>
          <Button text={i18next.t("log_in_btn")} color="primary" type="submit" />
        </MarginInline>
      </Form>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const token = data.get("token");

    const previousToken = this.props.settings.token;

    this.props.settings.token = token as string;

    try {
      await this.props.api.lookupSelf();
      route("/");
    } catch (e: unknown) {
      const error = e as Error;

      let errorMessage: string;

      if (error.message == "permission denied") {
        errorMessage = i18next.t("log_in_token_login_error");
      } else {
        errorMessage = error.message;
      }

      this.errorMessageRef.current.setErrorMessage(errorMessage);

      this.props.settings.token = previousToken;
    }
  }
}
