import { Button } from "../elements/Button";
import { Form } from "../elements/forms/Form";
import { InputWithTitle } from "../elements/InputWithTitle";
import { JSX } from "preact/jsx-runtime";
import { MarginInline } from "../elements/MarginInline";
import { PasswordInput } from "../elements/forms/PasswordInput";
import i18next from "i18next";

type FormUnsealProps = {
  onKeySubmit: (key: string) => void;
};

export function UnsealForm(props: FormUnsealProps): JSX.Element {
  return (
    <Form
      onSubmit={(data: FormData) => {
        props.onKeySubmit(data.get("unsealKey") as string);
      }}
    >
      <MarginInline>
        <InputWithTitle title={i18next.t("unseal_key_input_placeholder")}>
          <PasswordInput
            name="unsealKey"
            placeholder={i18next.t("unseal_key_input_placeholder")}
            required
          />
        </InputWithTitle>
      </MarginInline>

      <MarginInline>
        <Button text={i18next.t("unseal_submit_key_btn")} color="primary" type="submit" />
      </MarginInline>
    </Form>
  );
}
