import { Button } from "../elements/Button";
import { Component, JSX, createRef } from "preact";
import { CopyableInputBox } from "../elements/CopyableInputBox";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { Form } from "../elements/forms/Form";
import { Margin } from "../elements/Margin";
import { PageTitle } from "../elements/PageTitle";
import { Select, SelectOption } from "../elements/forms/Select";
import i18next from "i18next";

const passwordLengthMin = 1;
const passwordLengthMax = 64;
const passwordLengthDefault = 24;

function random() {
  if (
    typeof window.crypto?.getRandomValues === "function" &&
    typeof window.Uint32Array === "function"
  ) {
    return window.crypto.getRandomValues(new Uint32Array(1))[0] / 4294967295;
  }

  return Math.random();
}

const lowerCase = "abcdefghijklmnopqrstuvwxyz";
const upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const numbers = "1234567890";
const special = "!#$%&()*+,-./:;<=>?@[]^_{|}~";

const alphabets = {
  SECURE: lowerCase + upperCase + numbers + special,
  SMOL: lowerCase + numbers,
  SECURE_ISH: lowerCase + upperCase + numbers,
  HEX: "123456789ABCDEF",
};

const passwordOptionsDefault = {
  length: passwordLengthDefault,
  alphabet: alphabets.SECURE,
};

function genPassword(options = passwordOptionsDefault) {
  let pw = "";
  options = { ...passwordOptionsDefault, ...options };
  const pwArray = options.alphabet.split("");
  for (let i = 0; i < options.length; i++) {
    pw = pw.concat(pwArray[Math.floor(random() * pwArray.length)]);
  }
  return pw;
}

type PasswordGeneratorState = {
  length: number;
  alphabet: string;
};

export class PasswordGenerator extends Component<DefaultPageProps, PasswordGeneratorState> {
  constructor() {
    super();
    this.state = {
      length: passwordOptionsDefault.length,
      alphabet: passwordOptionsDefault.alphabet,
    };
  }

  passwordLengthSlider = createRef<HTMLInputElement>();
  alphabetSelector = createRef<HTMLSelectElement>();

  getPasswordLengthText(length: number): string {
    return i18next.t("password_generator_length_title", {
      min: length,
      max: passwordLengthMax,
    });
  }

  updateAlphabet(): void {
    this.setState({
      alphabet: this.alphabetSelector.current.value,
    });
  }

  updateLength(): void {
    this.setState({
      length: parseInt(this.passwordLengthSlider.current.value, 10),
    });
  }

  onSubmit(): void {
    this.updateLength();
    this.updateAlphabet();
  }

  render(): JSX.Element {
    return (
      <>
        <PageTitle title={i18next.t("password_generator_title")} />
        <Form onSubmit={() => this.onSubmit()}>
          <Margin>
            <h4>{this.getPasswordLengthText(this.state.length)}</h4>
          </Margin>
          <Margin>
            <input
              class="uk-range uk-form-width-medium"
              name="length"
              type="range"
              value={this.state.length}
              max={passwordLengthMax.toString()}
              min={passwordLengthMin.toString()}
              ref={this.passwordLengthSlider}
              onInput={() => {
                this.updateLength();
              }}
            />
          </Margin>
          <Margin>
            <Select
              selectRef={this.alphabetSelector}
              onChange={() => {
                this.updateAlphabet();
              }}
            >
              <SelectOption name="a-z a-Z 0-9 specials" value={alphabets.SECURE} />
              <SelectOption name="a-z a-Z 0-9" value={alphabets.SECURE_ISH} />
              <SelectOption name="a-z 0-9" value={alphabets.SMOL} />
              <SelectOption name="A-F 1-9" value={alphabets.HEX} />
            </Select>
          </Margin>

          <CopyableInputBox
            text={genPassword({
              length: this.state.length,
              alphabet: this.state.alphabet,
            })}
            copyable
          />
          <Margin>
            <Button
              text={i18next.t("password_generator_generate_btn")}
              color="primary"
              type="submit"
            />
          </Margin>
        </Form>
      </>
    );
  }
}
