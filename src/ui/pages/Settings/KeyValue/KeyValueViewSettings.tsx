import { Checkbox } from "../../../elements/forms/Checkbox";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { NumberInput } from "../../../elements/forms/NumberInput";
import { Select, SelectOption } from "../../../elements/forms/Select";
import { SupportedLanguages } from "../../../../utils/dataInterchange";
import { settingsSavedNotification } from "../Settings";
import i18next from "i18next";

export class KeyValueViewSettings extends Component<DefaultPageProps> {
  viewSyntaxSelectRef = createRef<HTMLSelectElement>();
  viewIndentInputRef = createRef<HTMLInputElement>();
  hybridModeInputRef = createRef<HTMLInputElement>();

  render() {
    return (
      <div>
        {/* KV View Language */}
        <InputWithTitle title={i18next.t("settings_kv_default_view_language")}>
          <Select
            selectRef={this.viewSyntaxSelectRef}
            onChange={() => {
              this.props.settings.kvViewDefaultLanguage = this.viewSyntaxSelectRef.current.value;
              settingsSavedNotification();
            }}
          >
            {SupportedLanguages.map((lang) => {
              return (
                <SelectOption
                  name={lang.readable}
                  value={lang.name}
                  selected={this.props.settings.kvViewDefaultLanguage == lang.name}
                />
              );
            })}
          </Select>
        </InputWithTitle>

        {/* KV View Indent */}
        <InputWithTitle title={i18next.t("settings_kv_view_indent")}>
          <NumberInput
            inputRef={this.viewIndentInputRef}
            value={String(this.props.settings.kvViewIndent).toString()}
            onChange={() => {
              const value = this.viewIndentInputRef.current.value;
              const indent = parseInt(value);
              this.props.settings.kvViewIndent = indent;
              settingsSavedNotification();
            }}
          />
        </InputWithTitle>

        {/* Always view in code mode */}
        <InputWithTitle title={i18next.t("settings_kv_use_hybrid_mode")}>
          <Checkbox
            checkboxRef={this.hybridModeInputRef}
            checked={this.props.settings.kvUseHybridView}
            onChange={() => {
              const value = this.hybridModeInputRef.current.checked;
              this.props.settings.kvUseHybridView = value;
              settingsSavedNotification();
            }}
          />
        </InputWithTitle>
      </div>
    );
  }
}
