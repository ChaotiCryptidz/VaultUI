import { Checkbox } from "../../../elements/forms/Checkbox";
import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { KeyValueEditorSettings } from "./KeyValueEditorSettings";
import { KeyValueViewSettings } from "./KeyValueViewSettings";
import { TextInput } from "../../../elements/forms/TextInput";
import { settingsSavedNotification } from "../Settings";
import i18next from "i18next";

export class KeyValueSettings extends Component<DefaultPageProps> {
  codeModeToggleRef = createRef<HTMLInputElement>();
  hideKeysRef = createRef<HTMLInputElement>();

  render() {
    return (
      <div>
        <h4>{i18next.t("settings_kv_title")}</h4>

        <KeyValueViewSettings {...this.props} />
        <KeyValueEditorSettings {...this.props} />

        {/* Always view in code mode */}
        <InputWithTitle title={i18next.t("settings_kv_always_view_in_code_mode")}>
          <Checkbox
            checkboxRef={this.codeModeToggleRef}
            checked={this.props.settings.kvAlwaysCodeView}
            onChange={() => {
              const value = this.codeModeToggleRef.current.checked;
              this.props.settings.kvAlwaysCodeView = value;
              settingsSavedNotification();
            }}
          />
        </InputWithTitle>

        {/* hide values on keys */}
        <InputWithTitle title={i18next.t("settings_kv_hide_values")}>
          <TextInput
            inputRef={this.hideKeysRef}
            value={this.props.settings.kvHideKeyValues.join(",")}
            onChange={() => {
              const value = this.hideKeysRef.current.value;
              this.props.settings.kvHideKeyValues = value;
              settingsSavedNotification();
            }}
          />
        </InputWithTitle>
      </div>
    );
  }
}
