import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { NumberInput } from "../../../elements/forms/NumberInput";
import { Select, SelectOption } from "../../../elements/forms/Select";
import { SupportedLanguages } from "../../../../utils/dataInterchange";
import { settingsSavedNotification } from "../Settings";
import i18next from "i18next";

export class KeyValueEditorSettings extends Component<DefaultPageProps> {
  editorSyntaxSelectRef = createRef<HTMLSelectElement>();
  editorIndentInputRef = createRef<HTMLInputElement>();
  render() {
    return (
      <div>
        {/* KV Editor Language */}
        <InputWithTitle title={i18next.t("settings_kv_default_editor_language")}>
          <Select
            selectRef={this.editorSyntaxSelectRef}
            onChange={() => {
              this.props.settings.kvEditorDefaultLanguage =
                this.editorSyntaxSelectRef.current.value;
              settingsSavedNotification();
            }}
          >
            {SupportedLanguages.map((lang) => {
              return (
                <SelectOption
                  name={lang.readable}
                  value={lang.name}
                  selected={this.props.settings.kvEditorDefaultLanguage == lang.name}
                />
              );
            })}
          </Select>
        </InputWithTitle>

        {/* KV Editor Indent */}
        <InputWithTitle title={i18next.t("settings_kv_editor_indent")}>
          <NumberInput
            inputRef={this.editorIndentInputRef}
            value={String(this.props.settings.kvEditorIndent).toString()}
            onChange={() => {
              const value = this.editorIndentInputRef.current.value;
              const indent = parseInt(value);
              this.props.settings.kvEditorIndent = indent;
              settingsSavedNotification();
            }}
          />
        </InputWithTitle>
      </div>
    );
  }
}
