import { Component } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { LanguageSetting } from "./LanguageSetting";
import { PageDirectionSetting } from "./PageDirectionSetting";
import { ThemeSetting } from "./ThemeSetting";
import { VaultURLSetting } from "./VaultURLSetting";
import i18next from "i18next";

export class GeneralSettings extends Component<DefaultPageProps> {
  render() {
    return (
      <div>
        <h4>{i18next.t("settings_general_title")}</h4>
        <ThemeSetting {...this.props} />
        <VaultURLSetting {...this.props} />
        <LanguageSetting {...this.props} />
        <PageDirectionSetting {...this.props} />
      </div>
    );
  }
}
