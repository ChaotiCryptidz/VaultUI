import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { TextInput } from "../../../elements/forms/TextInput";
import { settingsSavedNotification } from "../Settings";
import i18next from "i18next";

export class VaultURLSetting extends Component<DefaultPageProps> {
  vaultURLInputRef = createRef<HTMLInputElement>();

  render() {
    return (
      <InputWithTitle title={i18next.t("settings_general_vault_url")}>
        <TextInput
          inputRef={this.vaultURLInputRef}
          value={this.props.settings.apiURL}
          onChange={() => {
            // TODO: check for api health to see if is valid api url.
            this.props.settings.apiURL = this.vaultURLInputRef.current.value;
            settingsSavedNotification();
          }}
        />
      </InputWithTitle>
    );
  }
}
