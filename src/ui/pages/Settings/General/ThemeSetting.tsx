import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Select, SelectOption } from "../../../elements/forms/Select";
import { settingsSavedNotification } from "../Settings";
import i18next from "i18next";

const Themes = [
  { name: "dark", readable: "Dark" },
  { name: "light", readable: "Light" },
];

export class ThemeSetting extends Component<DefaultPageProps> {
  themeSelectRef = createRef<HTMLSelectElement>();

  render() {
    return (
      <InputWithTitle title={i18next.t("settings_general_theme")}>
        <Select
          selectRef={this.themeSelectRef}
          onChange={() => {
            const newTheme = this.themeSelectRef.current.value;
            this.props.settings.theme = newTheme;
            settingsSavedNotification();
          }}
        >
          {Themes.map((theme) => {
            return (
              <SelectOption
                name={theme.readable}
                value={theme.name}
                selected={this.props.settings.theme == theme.name}
              />
            );
          })}
        </Select>
      </InputWithTitle>
    );
  }
}
