import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Select, SelectOption } from "../../../elements/forms/Select";
import { settingsSavedNotification } from "../Settings";
import i18next from "i18next";

export class PageDirectionSetting extends Component<DefaultPageProps> {
  pageDirectionRef = createRef<HTMLSelectElement>();

  render() {
    return (
      <InputWithTitle title={i18next.t("settings_general_page_direction")}>
        <Select
          selectRef={this.pageDirectionRef}
          onChange={() => {
            this.props.settings.pageDirection = this.pageDirectionRef.current.value;
            document.documentElement.dir = this.props.settings.pageDirection;
            settingsSavedNotification();
          }}
        >
          {[
            { name: "ltr", readable: i18next.t("settings_general_page_direction_ltr") },
            { name: "rtl", readable: i18next.t("settings_general_page_direction_rtl") },
          ].map((direction) => {
            return (
              <SelectOption
                name={direction.readable}
                value={direction.name}
                selected={this.props.settings.pageDirection == direction.name}
              />
            );
          })}
        </Select>
      </InputWithTitle>
    );
  }
}
