import { Component, createRef } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Select, SelectOption } from "../../../elements/forms/Select";
import { getTranslationCompletePercentage } from "../../../../utils/translationUtils";
import { settingsSavedNotification } from "../Settings";
import i18next from "i18next";
import translations from "../../../../translations/index.mjs";

export class LanguageSetting extends Component<DefaultPageProps> {
  languageSelectRef = createRef<HTMLSelectElement>();

  render() {
    return (
      <InputWithTitle title={i18next.t("settings_general_language")}>
        <Select
          selectRef={this.languageSelectRef}
          onChange={async () => {
            const language = this.languageSelectRef.current.value;
            this.props.settings.language = language;

            const t = await i18next.changeLanguage(language);
            this.props.settings.pageDirection = t("language_direction");
            window.location.reload();
            settingsSavedNotification();
          }}
        >
          {Object.getOwnPropertyNames(translations).map((languageID) => {
            // @ts-ignore
            const languageName = i18next.getFixedT(languageID, null)("language_name");
            const languageCompletionPercent = getTranslationCompletePercentage(languageID);
            const name = `${languageName} (${languageCompletionPercent})`;
            return (
              <SelectOption
                name={name}
                value={languageID}
                selected={this.props.settings.language == languageID}
              />
            );
          })}
        </Select>
      </InputWithTitle>
    );
  }
}
