import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { GeneralSettings } from "./General/GeneralSettings";
import { Grid, GridSizes } from "../../elements/Grid";
import { KeyValueSettings } from "./KeyValue/KeyValueSettings";
import { PageTitle } from "../../elements/PageTitle";
import UIkit from "uikit";
import i18next from "i18next";

export function settingsSavedNotification() {
  UIkit.notification(i18next.t("settings_saved_notification"), {
    status: "success",
    timeout: 1000,
  });
}

export class Settings extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title={i18next.t("settings_title")} />
        <Grid size={GridSizes.NORMAL}>
          <GeneralSettings {...this.props} />
          <KeyValueSettings {...this.props} />
          {/*<KVLoadExportSettings {...this.props} />*/}
        </Grid>
      </>
    );
  }
}
