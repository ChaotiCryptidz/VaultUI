// @ts-ignore
import translations from "../../translations/index.mjs";
// ts-unignore

import { Button } from "../elements/Button";
import { Component } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { Form } from "../elements/forms/Form";
import { Margin } from "../elements/Margin";
import { MarginInline } from "../elements/MarginInline";
import { PageTitle } from "../elements/PageTitle";
import { Select, SelectOption } from "../elements/forms/Select";
import { getTranslationCompletePercentage } from "../../utils/translationUtils";
import { route } from "preact-router";
import i18next from "i18next";

export class SetLanguage extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title={i18next.t("set_language_title")} />
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <Margin>
            <Select name="language">
              {Object.getOwnPropertyNames(translations).map((languageID) => {
                // @ts-ignore
                const languageName = i18next.getFixedT(languageID, null)("language_name");
                const languageCompletionPercent = getTranslationCompletePercentage(languageID);
                const name = `${languageName} (${languageCompletionPercent})`;
                return (
                  <SelectOption
                    name={name}
                    value={languageID}
                    selected={this.props.settings.language == languageID}
                  />
                );
              })}
            </Select>
          </Margin>
          <p class="uk-text-danger" id="errorText" />
          <MarginInline>
            <Button text={i18next.t("set_language_btn")} color="primary" type="submit" />
          </MarginInline>
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const language = data.get("language") as string;
    this.props.settings.language = language;

    const t = await i18next.changeLanguage(language);
    this.props.settings.pageDirection = t("language_direction");
    route("/");
    window.location.reload();
  }
}
