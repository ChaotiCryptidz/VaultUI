import { Button } from "../elements/Button";
import { Component, JSX, createRef } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { ErrorMessage } from "../elements/ErrorMessage";
import { Margin } from "../elements/Margin";
import { PageTitle } from "../elements/PageTitle";
import { addClipboardNotifications } from "../../utils/clipboardNotifs";
import { route } from "preact-router";
import ClipboardJS from "clipboard";
import i18next from "i18next";

export class CopyLink extends Component<{ text: string; data: string }, unknown> {
  linkRef = createRef<HTMLAnchorElement>();

  componentDidMount(): void {
    const clipboard = new ClipboardJS(this.linkRef.current);
    addClipboardNotifications(clipboard, 600);
  }

  render(): JSX.Element {
    return (
      <a ref={this.linkRef} data-clipboard-text={this.props.data}>
        {this.props.text}
      </a>
    );
  }
}

type MeState = {
  loaded: boolean;
  canSealVault: boolean;
};

export class Me extends Component<DefaultPageProps, MeState> {
  defaultState = { loaded: false, canSealVault: false };
  constructor() {
    super();
    this.state = this.defaultState;
  }

  errorMessageRef = createRef<ErrorMessage>();

  async componentDidMount() {
    let canSealVault = false;
    try {
      const caps = await this.props.api.getCapsPath("sys/seal");
      canSealVault = caps.includes("sudo") && caps.includes("update");
    } catch (e) {
      canSealVault = false;
    }

    this.setState({
      loaded: true,
      canSealVault: canSealVault,
    });
  }

  componentWillUnmount() {
    this.setState(this.defaultState);
  }

  render(): JSX.Element {
    return (
      this.state.loaded && (
        <>
          <PageTitle title={i18next.t("me_page_title")} />

          <Margin>
            <ErrorMessage ref={this.errorMessageRef} />
          </Margin>

          <ul class="uk-nav">
            <li>
              <a
                onClick={async () => {
                  this.props.settings.token = "";
                  route("/");
                }}
              >
                {i18next.t("me_log_out_btn")}
              </a>
            </li>
            <li>
              <CopyLink text={i18next.t("me_copy_token_btn")} data={this.props.settings.token} />
            </li>
            <li>
              <a
                onClick={async () => {
                  try {
                    await this.props.api.renewSelf();
                    route("/");
                  } catch (e: unknown) {
                    const error = e as Error;
                    this.errorMessageRef.current.setErrorMessage(error.message);
                  }
                }}
              >
                {i18next.t("me_renew_lease_btn")}
              </a>
            </li>
            {this.state.canSealVault && (
              <li>
                <a
                  onClick={async () => {
                    try {
                      await this.props.api.sealVault();
                      route("/unseal", true);
                    } catch (e: unknown) {
                      const error = e as Error;
                      this.errorMessageRef.current.setErrorMessage(error.message);
                    }
                  }}
                >
                  {i18next.t("me_seal_vault_btn")}
                </a>
              </li>
            )}

            <br />

            <Button text={i18next.t("me_settings_btn")} color="primary" route="/settings" />
          </ul>
        </>
      )
    );
  }
}
