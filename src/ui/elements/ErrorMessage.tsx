import { Component, createRef } from "preact";
import UIkit from "uikit";
import i18next from "i18next";

type ErrorMessageState = {
  errorMessage: string;
};

// Use only when a error text element can't be used
// e.g inside componentDidMount
export function sendErrorNotification(errorMessage: string) {
  UIkit.notification({
    message: `Error: ${errorMessage}`,
    status: "danger",
    pos: "top-center",
    timeout: 2000,
  });
}

export function notImplementedNotification(): void {
  sendErrorNotification(i18next.t("not_implemented"));
}

export class ErrorMessage extends Component<unknown, ErrorMessageState> {
  public setErrorMessage(errorMessage: string) {
    this.setState({
      errorMessage: `Error: ${errorMessage}`,
    });

    sendErrorNotification(errorMessage);

    // make browser focus on the change.
    this.errorMessageRef.current.focus();
  }

  public clear() {
    this.setState({ errorMessage: "" });
  }

  errorMessageRef = createRef<HTMLParagraphElement>();

  render() {
    return (
      <p
        ref={this.errorMessageRef}
        {
          ...[] /* keeping for backwards compatability with seterrorMessage*/
        }
        {
          ...[] /* TODO: remove when finished removing all references to seterrorMessage */
        }
        id="errorMessage"
        class="uk-text-danger"
        {
          ...[] /* makes screenreaders read out changes to this element's content*/
        }
        aria-live="assertive"
      >
        {this.state.errorMessage || ""}
      </p>
    );
  }
}
