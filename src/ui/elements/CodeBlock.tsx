import { JSX } from "preact";
import Prism from "prismjs";

export type CodeBlockProps = {
  code: string;
  language?: string;
};

export function CodeBlock(props: CodeBlockProps): JSX.Element {
  if (props.language) {
    const highlightedCode = Prism.highlight(
      props.code,
      Prism.languages[props.language],
      props.language,
    );

    return (
      <pre
        class={"code-block " + "language-" + props.language}
        dir="ltr"
        dangerouslySetInnerHTML={{ __html: highlightedCode }}
      />
    );
  } else {
    return <pre class="code-block" dir="ltr" dangerouslySetInnerHTML={{ __html: props.code }} />;
  }
}
