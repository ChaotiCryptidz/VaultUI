import { Component, JSX, createRef } from "preact";
import { MarginInline } from "./MarginInline";
import { addClipboardNotifications } from "../../utils/clipboardNotifs";
import ClipboardJS from "clipboard";
import i18next from "i18next";

export type CopyableInputBoxProps = {
  text: string;
  copyable?: boolean;
  hideLikePassword?: boolean;
};

export class CopyableInputBox extends Component<CopyableInputBoxProps, unknown> {
  copyIconRef = createRef<HTMLAnchorElement>();

  componentDidMount(): void {
    const clipboard = new ClipboardJS(this.copyIconRef.current);
    addClipboardNotifications(clipboard, 600);
  }

  render(): JSX.Element {
    let type = "text";
    if (this.props.hideLikePassword) type = "password";

    return (
      <div>
        <MarginInline>
          {this.props.copyable && (
            <a
              ref={this.copyIconRef}
              class="uk-form-icon"
              uk-icon="icon: copy"
              role="img"
              data-clipboard-text={this.props.text}
              aria-label={i18next.t("copy_input_box_copy_icon_text")}
            />
          )}
          <input class="uk-input uk-input-copyable" type={type} value={this.props.text} readonly />
        </MarginInline>
      </div>
    );
  }
}
