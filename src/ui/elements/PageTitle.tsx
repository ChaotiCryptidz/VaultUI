import { JSX } from "preact";

export type PageTitleProps = {
  title: string;
};

export function PageTitle(props: PageTitleProps): JSX.Element {
  return (
    <h3 class="uk-card-title" id="pageTitle">
      {props.title}
    </h3>
  );
}
