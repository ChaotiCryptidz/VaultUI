import { JSX, RefObject } from "preact";

export type ButtonProps = {
  color: "primary" | "danger" | "secondary";
  text: string;
  // Either one or the other, pref on route when possible.
  route?: string;
  onClick?: () => unknown;
  type?: "submit" | null;
  buttonRef?: RefObject<HTMLButtonElement>;
};

export function Button(props: ButtonProps): JSX.Element {
  const classes = "uk-button " + ("uk-button-" + props.color);
  if (props.route) {
    return (
      <a class={classes} href={props.route} type={props.type}>
        {props.text}
      </a>
    );
  } else {
    return (
      <button
        {...props}
        ref={props.buttonRef}
        class={classes}
        type={props.type}
        onClick={props.onClick}
      >
        {props.text}
      </button>
    );
  }
}
