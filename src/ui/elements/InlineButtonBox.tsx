import { JSX } from "preact";

export type InlineButtonBoxProps = {
  children?: JSX.Element | JSX.Element[];
};

export function InlineButtonBox(props: InlineButtonBoxProps): JSX.Element {
  return <div>{props.children}</div>;
}
