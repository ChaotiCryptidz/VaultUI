import { JSX } from "preact";
import { getCurrentUrl, route } from "preact-router";
import i18next from "i18next";

export function NavBar(): JSX.Element {
  return (
    <nav class="uk-navbar uk-navbar-container">
      <div class="uk-navbar-left">
        <ul class="uk-navbar-nav">
          <li>
            <a href="/">{i18next.t("home_btn")}</a>
          </li>
          <li>
            <a
              onClick={async () => {
                window.history.back();
                // maybe???
              }}
            >
              {i18next.t("back_btn")}
            </a>
          </li>
          <li>
            <a
              onClick={async () => {
                route(getCurrentUrl(), false);
              }}
            >
              {i18next.t("refresh_btn")}
            </a>
          </li>
        </ul>
      </div>
      <div class="uk-navbar-right">
        <ul class="uk-navbar-nav">
          <li>
            <a href="/me">{i18next.t("me_btn")}</a>
          </li>
        </ul>
      </div>
    </nav>
  );
}
