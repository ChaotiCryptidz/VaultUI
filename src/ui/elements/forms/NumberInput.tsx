import { InputProps } from "./InputProps";
import { JSX, RefObject } from "preact";

type NumberInputProps = InputProps & {
  inputRef?: RefObject<HTMLInputElement>;
};

export function NumberInput(props: NumberInputProps): JSX.Element {
  return (
    <input class="uk-input uk-form-width-medium" type="number" ref={props.inputRef} {...props} />
  );
}
