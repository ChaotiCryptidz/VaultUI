import { InputProps } from "./InputProps";
import { JSX, RefObject } from "preact";

type TextInputProps = InputProps & {
  inputRef?: RefObject<HTMLInputElement>;
};

export function TextInput(props: TextInputProps): JSX.Element {
  return (
    <input class="uk-input uk-form-width-medium" type="text" ref={props.inputRef} {...props} />
  );
}
