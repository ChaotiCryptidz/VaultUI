import { InputProps } from "./InputProps";
import { JSX, RefObject } from "preact";

type TextInputProps = InputProps & {
  textAreaRef?: RefObject<HTMLInputElement>;
};

export function TextArea(props: TextInputProps): JSX.Element {
  return (
    <textarea class="uk-textarea uk-form-width-medium" textAreaRef={props.textAreaRef} {...props} />
  );
}
