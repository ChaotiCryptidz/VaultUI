export type InputProps = {
  name?: string;
  value?: string;
  placeholder?: string;
  onInput?: () => void;
  onChange?: () => void;
  required?: boolean;
  checked?: boolean;
};
