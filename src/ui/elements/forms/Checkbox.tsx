import { InputProps } from "./InputProps";
import { JSX, RefObject } from "preact";

type CheckboxProps = InputProps & {
  checkboxRef?: RefObject<HTMLInputElement>;
};

export function Checkbox(props: CheckboxProps): JSX.Element {
  return <input class="uk-checkbox" type="checkbox" ref={props.checkboxRef} {...props} />;
}
