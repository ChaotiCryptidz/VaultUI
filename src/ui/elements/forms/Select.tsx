import { InputProps } from "./InputProps";
import { JSX, RefObject } from "preact";

type SelectProps = InputProps & {
  selectRef?: RefObject<HTMLSelectElement>;
  children?: JSX.Element | JSX.Element[];
};

export function Select(props: SelectProps): JSX.Element {
  return (
    <select class="uk-select uk-form-width-medium" ref={props.selectRef} {...props}>
      {props.children}
    </select>
  );
}

type SelectOptionProps = {
  name: string;
  value: string;
  selected?: boolean;
};

export function SelectOption(props: SelectOptionProps): JSX.Element {
  return (
    <option label={props.name} value={props.value} selected={props.selected}>
      {props.name}
    </option>
  );
}
