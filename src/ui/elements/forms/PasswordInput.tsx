import { InputProps } from "./InputProps";
import { JSX, RefObject } from "preact";

type PasswordInputProps = InputProps & {
  inputRef?: RefObject<HTMLInputElement>;
};

export function PasswordInput(props: PasswordInputProps): JSX.Element {
  return (
    <input class="uk-input uk-form-width-medium" type="password" ref={props.inputRef} {...props} />
  );
}
