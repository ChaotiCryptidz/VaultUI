// @ts-ignore
import translations from "../translations/index.mjs";

export function arrayDiff(
  a: string[],
  b: string[],
): { missing: string[]; extra: string[]; common: string[] } {
  return {
    common: a.filter((x) => b.includes(x)),
    missing: a.filter((x) => !b.includes(x)),
    extra: b.filter((x) => !a.includes(x)),
  };
}

export function getTranslationKeys(translation_id: string): string[] {
  return [
    ...new Map(Object.entries(translations[translation_id] as Record<string, unknown>)).keys(),
  ];
}

export function getTranslationCompletePercentage(translation_id: string): string {
  const en_keys = getTranslationKeys("en");
  const translation_keys = getTranslationKeys(translation_id);
  const diff_between = arrayDiff(en_keys, translation_keys);

  const percent_num = Math.round(
    ((en_keys.length - diff_between.missing.length) / en_keys.length) * 100,
  );

  return `${percent_num}%`;
}
