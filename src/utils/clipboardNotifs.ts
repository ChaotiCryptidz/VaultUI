import UIkit from "uikit";
import i18next from "i18next";

export function addClipboardNotifications(clipboard: ClipboardJS, timeout = 1000): void {
  clipboard.on("success", () => {
    UIkit.notification(i18next.t("notification_copy_success"), {
      status: "success",
      timeout: timeout,
    });
  });
  clipboard.on("error", function (e: Error) {
    UIkit.notification(
      i18next.t("notification_copy_error", {
        error: e.message,
      }),
      {
        status: "danger",
        timeout: timeout,
      },
    );
  });
}
