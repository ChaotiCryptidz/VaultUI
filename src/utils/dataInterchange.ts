import JSON5 from "json5";
import yaml from "js-yaml";

export const SupportedLanguages = [
  { name: "json", readable: "JSON" },
  { name: "json5", readable: "JSON5" },
  { name: "yaml", readable: "YAML" },
];

export const LanguagePrismBlockMap: Record<string, string> = {
  json: "json",
  json5: "json5",
  yaml: "yaml",
};

export function toPrismCode(syntax: string): string {
  return LanguagePrismBlockMap[syntax];
}

export function parseData(data: string, syntax = "json"): Record<string, unknown> {
  if (syntax == "json") {
    return JSON.parse(data) as Record<string, unknown>;
  } else if (syntax == "json5") {
    return JSON5.parse(data);
  } else if (syntax == "yaml") {
    return yaml.load(data) as Record<string, unknown>;
  }
}

export function dumpData(data: Record<string, unknown>, space = 4, syntax = "json"): string {
  if (syntax == "json") {
    return JSON.stringify(data, null, space);
  } else if (syntax == "json5") {
    return JSON5.stringify(data, null, space);
  } else if (syntax == "yaml") {
    return yaml.dump(data, {
      indent: space,
      quotingType: '"',
      forceQuotes: true,
    });
  }
}
