import { Settings } from "./settings/Settings";
import { settings } from "./globals/globalSettings";

// Playground is a way to debug and test things.
// Anything you put in here is gonna be run on page initial load
// before rendering.
// Also it only runs when process.env.NODE_ENV == "development"

declare global {
  interface Window {
    settings: Settings;
  }
}

// Please empty this function before committing.
export async function playground(): Promise<void> {
  console.log("Welcome to Playground!");

  window.settings = settings;
}
