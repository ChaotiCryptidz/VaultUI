export type NewTOTPData = {
  name: string;
  generate: boolean;
  exported?: boolean;
  key_size?: number;
  url?: string;
  key?: string;
  issuer?: string;
  account_name?: string;
  period?: number | string;
  algorithm?: string;
  digits?: number;
};

export type NewTOTPResp = {
  url: string;
  barcode: string;
};
