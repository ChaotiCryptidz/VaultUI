export type CapabilitiesType = {
  [path: string]: string[];
  capabilities?: string[];
};
