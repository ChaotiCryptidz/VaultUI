export type MountType = {
  type: string;
  options: {
    version: string;
  };
};

export type MountsType = {
  [key: string]: MountType;
};

export type NewMountParams = {
  name: string;
  type: string;
  options?: {
    version: string;
  };
};
