export type SealStatusType = {
  progress: number;
  t: number;
  sealed: boolean;
};
