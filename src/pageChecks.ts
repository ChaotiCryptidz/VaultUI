import { API } from "./api/API";
import { Settings } from "./settings/Settings";
import { route } from "preact-router";

export async function pageChecks(url: string, api: API, settings: Settings): Promise<boolean> {
  if (url.startsWith("/set_language")) return;
  if (url.startsWith("/me")) return;
  if (url.startsWith("/settings")) return;
  if (url.startsWith("/pw_gen")) return;

  if (settings.language.length == 0) {
    route("/set_language", true);
    return true;
  }

  if (url.startsWith("/set_vault_url")) return;

  if (settings.apiURL.length == 0) {
    route("/set_vault_url", false);
    return true;
  }

  if (url.startsWith("/unseal")) return;

  const sealStatus = await api.getSealStatus();
  if (sealStatus.sealed) {
    route("/unseal", true);
    return true;
  }

  if (url.startsWith("/login")) return;

  try {
    await api.lookupSelf();
  } catch (e) {
    route("/login", true);
    return true;
  }
  return false;
}
