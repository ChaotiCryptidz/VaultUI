import { StorageType } from "./storage/StorageType";
import { default_theme } from "../ThemeLoader";

type OnChangeListener = (key: string) => void;

// Documenting this here as Settings class is too big and clunky
export type SettingsData = {
  // Base URL for Vault API
  apiURL: string;
  // User's Token
  token: string;
  // Page Direction (ltr, rtl)
  pageDirection: string;
  // Language code for all UI text
  language: string;
  // Theme (light, dark)
  theme: string;
  // Always view K/V secrets as a code block
  kvAlwaysCodeView: boolean;
  // Default language on K/V editor page
  kvEditorDefaultLanguage: string;
  // Default indentation on K/V editor page
  kvEditorIndent: number;
  // Default language on K/V view page
  kvViewDefaultLanguage: string;
  // Default indent on K/V indent page
  kvViewIndent: number;
  // Keys to hide values on (setting input type=password) like *****
  kvHideKeyValues: string[];
};

export class Settings {
  constructor() {
    this.storage = localStorage;
    this.listeners = [];
  }

  private storage: StorageType;
  private listeners: OnChangeListener[];

  registerListener(listener: OnChangeListener) {
    this.listeners.push(listener);
  }

  alertChange(key: string) {
    for (const listener of this.listeners) {
      listener(key);
    }
  }

  // Partial as token and apiURL are not saved.
  dumpSettings(): Partial<SettingsData> {
    return {
      pageDirection: this.pageDirection,
      language: this.language,
      theme: this.theme,
      kvAlwaysCodeView: this.kvAlwaysCodeView,
      kvEditorDefaultLanguage: this.kvEditorDefaultLanguage,
      kvEditorIndent: this.kvEditorIndent,
      kvViewDefaultLanguage: this.kvViewDefaultLanguage,
      kvViewIndent: this.kvViewIndent,
      kvHideKeyValues: this.kvHideKeyValues,
    };
  }

  loadSettings(settings: Partial<SettingsData>) {
    this.pageDirection = settings.pageDirection;
    this.language = settings.language;
    this.theme = settings.theme;
    this.kvAlwaysCodeView = settings.kvAlwaysCodeView;
    this.kvEditorDefaultLanguage = settings.kvEditorDefaultLanguage;
    this.kvEditorIndent = settings.kvEditorIndent;
    this.kvViewDefaultLanguage = settings.kvViewDefaultLanguage;
    this.kvViewIndent = settings.kvViewIndent;
    this.kvHideKeyValues = settings.kvHideKeyValues;
  }

  get apiURL(): string | null {
    const apiurl = this.storage.getItem("apiURL") || "";
    return apiurl;
  }
  set apiURL(value: string) {
    this.storage.setItem("apiURL", value);
    this.alertChange("apiURL");
  }

  get token(): string | null {
    const tok = this.storage.getItem("token") || "";
    return tok.length > 0 ? tok : null;
  }
  set token(value: string) {
    this.storage.setItem("token", value);
    this.alertChange("token");
  }

  get pageDirection(): string {
    return this.storage.getItem("pageDirection") || "ltr";
  }
  set pageDirection(value: string) {
    this.storage.setItem("pageDirection", value);
    this.alertChange("pageDirection");
  }

  get language(): string {
    return this.storage.getItem("language") || "";
  }
  set language(value: string) {
    this.storage.setItem("language", value);
    this.alertChange("language");
  }

  get theme(): string {
    return this.storage.getItem("theme") || default_theme;
  }
  set theme(value: string) {
    this.storage.setItem("theme", value);
    this.alertChange("theme");
  }

  get kvAlwaysCodeView(): boolean {
    const value = this.storage.getItem("kvAlwaysCodeView") || "false";
    return value == "true";
  }
  set kvAlwaysCodeView(value: boolean) {
    this.storage.setItem("kvAlwaysCodeView", String(value));
    this.alertChange("kvAlwaysCodeView");
  }

  get kvUseHybridView(): boolean {
    const value = this.storage.getItem("kvUseHybridView") || "true";
    return value == "true";
  }
  set kvUseHybridView(value: boolean) {
    this.storage.setItem("kvUseHybridView", String(value));
    this.alertChange("kvUseHybridView");
  }

  get kvEditorDefaultLanguage(): string {
    return this.storage.getItem("kvEditorDefaultLanguage") || "yaml";
  }
  set kvEditorDefaultLanguage(value: string) {
    this.storage.setItem("kvEditorDefaultLanguage", value);
    this.alertChange("kvEditorDefaultLanguage");
  }

  get kvEditorIndent(): number {
    const value = this.storage.getItem("kvEditorIndent");
    if (value) return parseInt(value);
    return 2;
  }
  set kvEditorIndent(value: number) {
    this.storage.setItem("kvEditorIndent", String(value));
    this.alertChange("kvEditorIndent");
  }

  get kvViewDefaultLanguage(): string {
    return this.storage.getItem("kvViewDefaultLanguage") || "yaml";
  }
  set kvViewDefaultLanguage(value: string) {
    this.storage.setItem("kvViewDefaultLanguage", value);
    this.alertChange("kvViewDefaultLanguage");
  }

  get kvViewIndent(): number {
    const value = this.storage.getItem("kvViewIndent");
    if (value) return parseInt(value);
    return 2;
  }
  set kvViewIndent(value: number) {
    this.storage.setItem("kvViewIndent", String(value));
    this.alertChange("kvViewIndent");
  }

  get kvHideKeyValues(): string[] {
    const value = this.storage.getItem("kvHideKeyValues") || "";
    return value.split(",");
  }
  set kvHideKeyValues(value: string[] | string) {
    if (typeof value === "string") {
      this.storage.setItem("kvHideKeyValues", value);
    } else {
      this.storage.setItem("kvHideKeyValues", value.join(","));
    }
    this.alertChange("kvHideKeyValues");
  }
}
