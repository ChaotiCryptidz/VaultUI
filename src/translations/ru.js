module.exports = {
  // The localised name for the language
  language_name: "русский",
  // Internal: The direction of text (ltr or rtl)
  language_direction: "ltr",

  // These are the buttons on the top bar.
  home_btn: "Главная страница",
  back_btn: "Назад",
  refresh_btn: "Обновить",
  me_btn: "Профиль/параметры",

  // Common buttons / placeholders
  common_new: "Новый",
  common_view: "Просмотр",
  common_back: "Назад",
  common_edit: "Редактировать",
  common_create: "Создать",
  common_delete: "Удалить",
  common_copy: "Копировать",
  common_username: "Имя пользователя",
  common_password: "Пароль",
  common_name: "Имя",

  // Common but for options inside secrets engines
  common_cidrs: "Привязанные CIDR",
  common_exp_max_ttl: "Явный максимальный срок действия",
  common_max_ttl: "Максимальный срок действия",
  common_initial_ttl: "Начальный срок действия",
  common_default_lease_ttl: "Срок действия выданных кодов по умолчанию",
  common_max_lease_ttl: "Максимальный срок действия выданных кодов",

  // General Notification Messages
  notification_copy_success: "Текст скопирован в буфер обмена.",
  not_implemented: "Функция ещё не реализована",

  // Copyable Modal
  copy_box_download_btn: "Загрузить",
  copy_box_copy_btn: "Копировать",
  copy_box_close_btn: "Закрыть",

  // Generic Loading Text
  content_loading: "Загрузка..",

  // Copyable Input Box
  copy_input_box_copy_icon_text: "Копировать",

  // File Upload Input
  file_upload_input_btn: "Загрузить файл",

  // Me Page
  me_page_title: "Профиль/параметры",
  me_log_out_btn: "Выход",
  me_seal_vault_btn: "Закрыть хранилище",
  me_copy_token_btn: "Копировать код доступа",
  me_renew_lease_btn: "Продлить действие кода доступа",
  me_change_language_btn: "Выбор языка",
  me_set_vault_url_btn: "Установить URL хранилища",

  // Home Page
  home_page_title: "Главная страница",
  home_vaulturl_text: "Адрес хранилища: {{text}}",
  home_password_generator_btn: "Генератор паролей",
  home_your_token_expires_in: "Продолжительность ключа: {{date, until_date}}",
  home_secrets_title: "Тайны",
  home_secrets_description: "Просмотр, создание и управление тайнами.",
  home_access_title: "Доступ",
  home_access_description:
    "Управление доступом к хранилищу и способами аутентификации пользователей.",
  home_policies_title: "Политика",
  home_policies_description: "Управление политикой и правами доступа.",

  // Secrets Home Page
  secrets_home_page_title: "Тайны",
  secrets_home_new_secrets_engine_button: "Новый обработчик тайн",

  // New Secrets Engine Page
  new_secrets_engine_title: "Новый обработчик тайн",
  new_secrets_engine_kv_title: "Ключ/значение",
  new_secrets_engine_kv_description: 'Для хранения тайн в формате пар "ключ/значение".',
  new_secrets_engine_totp_title: "TOTP",
  new_secrets_engine_totp_description:
    "Для хранения одноразовых кодов, создаваемых алгоритмом TOTP.",
  new_secrets_engine_transit_title: "Transit",
  new_secrets_engine_transit_description: "Для шифрования/расшифрования данных без их хранения.",

  // New KV Engine Page
  new_kv_engine_title: 'Новый обработчик пар "ключ/значение"',
  new_kv_engine_version_1: "Версия 1",
  new_kv_engine_version_2: "Версия 2",

  // New KV Engine Page
  new_totp_engine_title: "Новый обработчик TOTP",

  // New Transit Engine Page
  new_transit_engine_title: "Новый обработчик Transit",

  // Unseal Page
  unseal_vault_text: "Раскрыть хранилище",
  unseal_submit_key_btn: "Отправить ключ",
  unseal_input_btn: "Ввести ключ вручную",
  unseal_qr_btn: "Ввести ключ через QR-код",
  unseal_key_input_placeholder: "Ключ",
  unseal_keys_progress: "Ключи: {{progress}}/{{keys_needed}}",

  // Language Selector Page
  set_language_title: "Выбор языка",
  set_language_btn: "Выбрать язык",

  // Password Generator Page
  password_generator_title: "Генератор паролей",
  password_generator_length_title: "Длина пароля ({{min}}/{{max}})",
  password_generator_generate_btn: "Генерировать",

  // Login Page
  log_in_title: "Вход",
  log_in_with_token: "Вход с кодом доступа",
  log_in_with_username: "Вход по имени",
  log_in_token_input: "Код доступа",
  log_in_username_input: "Имя пользователя",
  log_in_password_input: "Пароль",
  log_in_btn: "Войти",
  log_in_token_login_error: "Неправильный код доступа",

  // Key Value Delete Page
  kv_delete_title: "Удаление ключа/значения",
  kv_delete_text: "Вы уверены, что хотите удалить это?",
  kv_delete_suffix: " (удалить)",

  // Key Value New Page
  kv_new_title: "Новый ключ/значение",
  kv_new_suffix: " (новый)",
  kv_new_path: "Относительный путь",

  // Key Value Secret Page
  kv_secret_title: "Тайные ключи/значения",
  kv_secret_deleted_text:
    "Тайная версия данных была удалена, но может быть восстановлена. Вы хотите восстановить её?",
  kv_secret_restore_btn: "Восстановить тайную версию",
  kv_secret_loading: "Загрузка тайных данных..",
  kv_secret_delete_btn: "Удалить",
  kv_secret_delete_all_btn: "Удалить все версии",
  kv_secret_delete_version_btn: "Удалить версию {{ version }}",
  kv_secret_versions_btn: "Версии",

  // Key Value Secret Editor Page
  kv_sec_edit_title: "Редактировать ключи/значения",
  kv_sec_edit_syntax: "Синтаксис",
  kv_sec_edit_loading: "Загрузка редактора..",
  kv_sec_edit_invalid_json_err: "Некорректный JSON",
  kv_sec_edit_suffix: " (редакт.)",

  // Key Value Secret Versions Page
  kv_sec_versions_title: "Версии ключей/значений",
  kv_sec_versions_suffix: " (версии)",

  // Key Value View/List Secrets Page
  kv_view_title: "Просмотр ключей/значений",
  kv_view_cubbyhole_text:
    "При использовании cubbyhole, тайные данные можно хранить до тех пор, пока действителен ваш код доступа. Они будут удалены при истечении кода и могут быть просмотрены только с использованием этого кода.",
  kv_view_new_btn: "Создать",
  kv_view_delete_btn: "Удалить",
  kv_view_none_here_text: "У вас на данный момент нет тайных данных. Хотите ли вы их создать?",
  kv_view_search_input_text: "Поиск",

  // TOTP New Page
  totp_list_empty: "У вас на данный момент нет кодов TOTP. Хотите ли вы их создать?",
  totp_list_search_input_text: "Поиск",

  // New TOTP Key Page
  totp_new_title: "Новый ключ TOTP",
  totp_new_suffix: " (новый)",
  totp_new_info:
    "Необходим либо ключ, либо адрес URI. Лучше всего использовать URI, но он может не сработать. Отсканируйте QR-код и скопируйте его адрес.",
  totp_new_uri_input: "URI",
  totp_new_key_input: "Ключ",
  totp_new_switch_to_qr_btn: "Использовать ввод QR-кода",
  totp_new_switch_back_to_manual_input_btn: "Использовать ручной ввод данных",

  totp_new_generated: "Новый сгенерированный ключ",
  totp_new_generated_suffix: "(новый сген.)",
  totp_new_generated_warning:
    'Не забудьте записать эту информацию в безопасном месте, так как снова её увидеть можно только при помощи "сырого" API Vault.',
  totp_new_generated_issuer: "Издатель",
  totp_new_generated_account_name: "Имя учётной записи",
  totp_new_generated_algorithm: "Алгоритм",
  totp_new_generated_key_size: "Размер ключа (байт)",
  totp_new_generated_period: "Период (время для обновления)",
  totp_new_generated_digits: "Цифры (длина 6/8)",
  totp_new_generated_export: "Показывать QR и URI (рекомендуется)",

  // TOTP Delete Page
  totp_delete_title: "Удалить ключ TOTP",
  totp_delete_suffix: " (удалить)",
  totp_delete_text: "Вы уверены, что хотите удалить эту тайну TOTP?",

  // Transit Commmon
  transit_encrypt: "Зашифровать",
  transit_decrypt: "Расшифровать",
  transit_rewrap: "Перешифровать",

  // Transit View Page
  transit_view_title: "Просмотр Transit",
  transit_view_none_here_text: "У вас нет ключей Transit, хотите ли вы их создать?",

  transit_new_key_title: "Новый ключ Transit",
  transit_new_key_suffix: " (новый)",

  // Transit View Secret Page
  transit_view_secret_title: "Просмотр тайных данных Transit",
  transit_view_encrypt_icon_text: "Значок шифрования",
  transit_view_encrypt_description: "Зашифровать текст или двоичные данные, закодированные base64.",
  transit_view_decrypt_description: "Расшифровать шифр-текст.",
  transit_view_decrypt_icon_text: "Значок расшифрования",
  transit_view_rewrap_description: "Перешифровать шифр-текст, используя другую версию ключа.",
  transit_view_rewrap_icon_text: "Значок перешифрования",

  // Transit Encrypt Page
  transit_encrypt_title: "Шифрование Transit",
  transit_encrypt_suffix: " (шифр.)",
  transit_encrypt_input_placeholder: "Текст или base64",
  transit_encrypt_already_encoded_checkbox: "Данные уже закодированы в base64?",
  transit_encrypt_encryption_result_title: "Результат шифрования",

  // Transit Decrypt Page
  transit_decrypt_title: "Расшифрование Transit",
  transit_decrypt_suffix: " (расшифр.)",
  transit_decrypt_input_placeholder: "Шифр-текст",
  transit_decrypt_decode_checkbox: "Нужно ли раскодировать текст из base64?",
  transit_decrypt_decryption_result_title: "Результат расшифрования",

  // Transit Rewrap Page
  transit_rewrap_title: "Перешифрование Transit",
  transit_rewrap_suffix: " (перешифр.)",
  transit_rewrap_version_option_text: "{{version_num}}",
  transit_rewrap_latest_version_option_text: "{{version_num}} (последняя версия)",
  transit_rewrap_input_placeholder: "Шифр-текст",
  transit_rewrap_result_title: "Результат перешифрования",

  // Delete Secret Engine Page
  delete_secrets_engine_title: "Удалить обработчик тайн ({{mount}})",
  delete_secrets_engine_message:
    "Вы уверены, что хотите удалить этот обработчик тайн и все содержащиеся в нём данные? Это действие необратимо.",

  // Access Home
  access_home_page_title: "Доступ",
  access_auth_methods_title: "Методы аутентификации",
  access_auth_methods_description: "Просмотр и управление разрешёнными методами аутентификации.",
  access_entities_title: "Сущности",
  access_entities_description:
    "Просмотр и управление пользователями и объектами, имеющих доступ к хранилищу.",
  access_groups_title: "Группы",
  access_groups_description: "Просмотр и управление группами сущностей.",
  access_leases_title: "Коды доступа",
  access_leases_description: "Просмотр и управление кодами доступа.",

  // Auth Home Page
  auth_home_title: "Аутентификация",
  auth_home_view_config: "Просмотр настроек",
  auth_home_edit_config: "Редактирование настроек",

  // Auth View Config Page
  auth_view_config_title: "Просмотр настроек аутентификации",
  auth_view_config_suffix: " (просм. настр.)",
  auth_view_config_type: "Тип",
  auth_view_config_path: "Путь",
  auth_view_config_description: "Описание",
  auth_view_config_accessor: "Ссылка ключа доступа",
  auth_view_config_local: "Локальный",
  auth_view_config_seal_wrap: "Дополнительное шифрование",
  auth_view_config_list_when_unauth: "Отображать без аутентификации?",
  auth_view_config_token_type: "Тип кода доступа",

  // UserPass Common
  auth_common_zero_default:
    "Когда в поле указано значение 0, будет использовано значение по умолчанию",
  auth_common_generated_tokens:
    "Эти параметры будут применяться к кодам доступа, создаваемым при входе в систему",
  auth_common_default_policy_attached: "Не применять политику по умолчанию",
  auth_common_max_token_uses: "Максимальное кол-во применений кодов доступа",
  auth_common_token_peroid: "Период (в секундах)",
  auth_common_policies: "Политика",
  auth_common_type: "Тип кодов доступа",
  auth_common_token_config: "Настройка кода доступа",

  // userpass Users List
  userpass_users_list_title: "Список пользователей",

  // userpass User View
  userpass_user_view_title: "Просмотр пользователя",

  // userpass user edit
  userpass_user_edit_title: "Редактирование пользователя",

  // userpass user new
  userpass_user_new_title: "Новый пользователь",

  userpass_user_delete_title: "Удаление пользователя",
  userpass_user_delete_text:
    "Вы уверены, что хотите удалить этого пользователя? Это действие нельзя отменить.",

  // Policies Home
  policies_home_title: "Политика",

  // Policy View
  policy_view_title: "Просмотр политики ({{policy}})",

  // Policy New
  policy_new_title: "Создание новой политики",
  policy_new_already_exists: "Данная политика уже существует.",

  policy_edit_title: "Редактирование политики ({{policy}})",

  // Policy Delete
  policy_delete_title: "Удаление политики ({{policy}})",
  policy_delete_text:
    "Вы уверены, что хотите удалить эту политику? Данное действие нельзя отменить, и есть возможность, что будут испорчены права доступа.",
};
