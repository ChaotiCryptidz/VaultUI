#!/usr/bin/env bash

nix flake update
nix develop --command yarn upgrade
nix develop --command yarn2nix > yarn.nix