const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: "development",
  cache: true,
  entry: './src/main.tsx',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  stats: {
    colors: true,
    timings: true,
  },
  plugins: [
    new HtmlWebpackPlugin({ title: "VaultUI" }),
    new webpack.DefinePlugin({
      BUILD_STRING: "DEPRECATED"
    })
  ],
  devServer: {
    open: process.env.BROWSER,
    historyApiFallback: true,
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.tsx', '.ts', '.js', ".mjs"],
    alias: {os: false}
  },

  module: {
    rules: [
      {
        assert: { type: "css" },
        loader: "css-loader",
        options: {
          exportType: "string",
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          "sass-loader"
        ],
      },
      {
        test: /\.ts(x?)$/,
        use: [
          {
            loader: 'ts-loader'
          }
        ]
      },
    ],
  },
};