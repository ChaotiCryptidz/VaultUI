{
  config,
  lib,
  pkgs,
  ...
}: let
  inherit (lib.modules) mkIf;
  inherit (lib.options) mkOption mkEnableOption mkPackageOption;
  inherit (lib) types;

  cfg = config.services.vaultui;
in {
  options = {
    services.vaultui = {
      enable = mkEnableOption "vaultui";
      package = mkPackageOption pkgs "vaultui" {};

      domain = mkOption {
        type = types.str;
      };

      path = mkOption {
        type = types.str;
        default = "/";
      };
    };
  };

  config = mkIf cfg.enable {
    services.nginx = {
      enable = true;

      virtualHosts."${cfg.domain}" = {
        forceSSL = true;
        enableACME = true;
        locations = {
          "${cfg.path}" = {
            root = "${cfg.package}/share/vaultui";
            extraConfig = ''
              try_files $uri $uri/ /index.html;
            '';
          };
        };
      };
    };
  };
}
