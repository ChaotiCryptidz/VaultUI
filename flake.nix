{
  description = "A Web UI for Hashicorp Vault";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    ...
  }:
    {
      nixosModules.vaultui = import ./nixos-module.nix;
      nixosModules.default = self.nixosModules.vaultui;

      overlays.vaultui = final: prev: {
        vaultui = final.mkYarnPackage rec {
          pname = "vaultui";
          version = "latest";
          src = ./.;

          yarnLock = ./yarn.lock;
          yarnNix = ./yarn.nix;
          packageJSON = ./package.json;

          doDist = false;

          # required for using system libsass, or else it tries to fetch new node headers
          yarnPreBuild = ''
            export npm_config_nodedir=${final.pkgs.nodejs}
          '';

          pkgConfig = {
            node-sass = {
              nativeBuildInputs = with final.pkgs; [pkg-config];
              buildInputs = with final.pkgs; [libsass python3];
              postInstall = ''
                LIBSASS_EXT=auto yarn --offline run build
                rm build/config.gypi
              '';
            };
          };

          buildPhase = ''
            runHook preBuild

            yarn run build

            runHook postBuild
          '';

          installPhase = ''
            runHook preInstall

            mkdir -p "$out/share/vaultui"
            ${final.rsync}/bin/rsync --recursive deps/vaultui/dist/ "$out/share/vaultui"

            runHook postInstall
          '';
        };
      };
      overlays.default = self.overlays.vaultui;
    }
    // utils.lib.eachSystem (utils.lib.defaultSystems) (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [self.overlays.default];
      };
    in {
      defaultPackage = self.packages."${system}".vaultui;
      packages.vaultui = pkgs.vaultui;

      devShell = pkgs.mkShell {
        LIBSASS_EXT = "auto";
        npm_config_nodedir = "${pkgs.nodejs}";

        nativeBuildInputs = with pkgs; [
          nodejs

          nodePackages.yarn
          yarn2nix

          # So we don't need to manually build libsass
          pkg-config
          libsass
          python3
        ];
      };
    });
}
