const path = require('path');
const webpack = require('webpack');
var os = require("os");

const TerserPlugin = require("terser-webpack-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

var babelOptions = {
  "presets": [
    [
      "@babel/preset-env",
      {
        "corejs": { "version": 3 },
        "useBuiltIns": "usage",
      }
    ]
  ],
  "plugins": [
    ["@babel/plugin-proposal-decorators", { "decoratorsBeforeExport": true }],
    ["@babel/plugin-transform-class-properties"],
    ["@babel/transform-runtime"],
    ["@babel/plugin-syntax-import-assertions"]
  ]
};

module.exports = {
  mode: "production",
  cache: false,
  entry: './src/main.tsx',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  stats: {
    colors: true,
    timings: true,
  },
  plugins: [
    new HtmlWebpackPlugin({ title: "VaultUI" }),
    new webpack.DefinePlugin({
      BUILD_STRING: "DEPRECATED"
    })
  ],
  devServer: {
    open: process.env.BROWSER || "microsoft-edge-dev",
    historyApiFallback: true,
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.tsx', '.ts', '.js', ".mjs"],
    alias: {os: false}
  },

  module: {
    rules: [
      {
        assert: { type: "css" },
        loader: "css-loader",
        options: {
          exportType: "string",
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          "sass-loader"
        ],
      },
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: babelOptions
          },
          {
            loader: 'ts-loader'
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: babelOptions
          }
        ]
      }
    ],
  },

  optimization: {
    minimize: true,
    minimizer: [
      `...`,
      new TerserPlugin({
        terserOptions: {
          ecma: "2015"
        }
      }),
      new CssMinimizerPlugin(),
    ],
  },
};
